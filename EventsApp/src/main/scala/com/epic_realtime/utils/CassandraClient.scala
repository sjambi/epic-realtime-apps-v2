package com.epic_realtime.utils

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Row
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.ResultSetFuture
import com.datastax.driver.core.Session
import scala.collection.JavaConversions._
import com.datastax.driver.core.Metadata
import com.typesafe.config.{ Config, ConfigFactory }


/**
 * Simple cassandra client, following the datastax documentation
 * (http://www.datastax.com/documentation/developer/java-driver/2.0/java-driver/quick_start/qsSimpleClientCreate_t.html).
 */
class CassandraClient() {

  val config = ConfigFactory.load() 
  val node = config.getString("cassandra.host") 

  // Connect directly to Cassandra from the driver
  private val cluster = Cluster.builder().addContactPoint(node).build()
  // log(cluster.getMetadata())
  val session = cluster.connect()
  println(s"Connected to cluster ${cluster.getMetadata().getClusterName()}")

  // private def log(metadata: Metadata): Unit = {
  //   Logger.info(s"Connected to cluster: ${metadata.getClusterName}")
  //   for (host <- metadata.getAllHosts()) {
  //     Logger.info(s"Datatacenter: ${host.getDatacenter()}; Host: ${host.getAddress()}; Rack: ${host.getRack()}")
  //   }
  // }

  def createSchema(): Unit = {
    // Create the keyspace if not exists
    session.execute(
      """CREATE KEYSPACE IF NOT EXISTS epic_realtime 
        WITH replication = {'class': 'NetworkTopologyStrategy', 'datacenter1': '1'};""")
    // Execute statements to create two tables if not exist, events and tweets
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.events (
        event_name text PRIMARY KEY,
        event_description text,
        start_date text,
        end_date text,
        is_active boolean,
        keywords set<text>
        );""")
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.tweets (
        event_name text,
        created_at timestamp,
        tweet_id text,
        tweet_json text,
        PRIMARY KEY (event_name, created_at, tweet_id));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS tweets_tweet_id 
        ON epic_realtime.tweets (tweet_id);""")
  }


  def addEvent(event_name: String, description: String, startDate: String, endDate: String, isActive: Boolean) = {
    session.execute(
      "INSERT INTO epic_realtime.events (event_name, event_description, start_date, end_date, is_active) " +
      "VALUES (" +
        "'" + event_name + "'," +
        "'" + description + "'," +
        "'" + startDate + "'," +
        "'" + endDate + "'," +
        "" + isActive + "" +
        ");")
  }

  def getEvent(event_name: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.events WHERE event_name ='" + event_name + "';")
  }

  def deleteEvent(event_name: String) = {
    session.execute("DELETE FROM epic_realtime.events WHERE event_name ='" + event_name + "';")
  }

  // def DeleteEvent(event_name: String) = {
  //   Statement statement = QueryBuilder.delete()
  //     .from("epic_realtime", "events")
  //     .where(eq("event_name", event_name))
  //   session.execute(statement)
  // }

  def getTweetCount(event_name: String): Long = {
    session.execute(s"select count(*) from epic_realtime.tweets where event_name ='$event_name'").one.getLong(0)
  }

  def getEvents(): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.events;")
  }
  // def SetEventActiveStatus(event_name: String, isActive: Boolean) = {
  //   val statement = QueryBuilder.update("epic_realtime", "events")
  //     .`with`(QueryBuilder.set("isActive", isActive))
  //     .where(QueryBuilder.eq("event_name", event_name))
  //   session.execute(statement)
  // }

  def setEventActiveStatus(event_name: String, isActive: Boolean, endDate: String) = {
    session.execute("UPDATE epic_realtime.events SET is_active = " + isActive + ", end_date = '" + endDate + "' WHERE event_name ='" + event_name + "';")
  }

  def getKeywords(event_name: String): Row = {
    session.execute("SELECT keywords FROM epic_realtime.events WHERE event_name ='" + event_name + "';").one
  }

  def addKeyword(event_name: String, keyword: String) = {
    session.execute("UPDATE epic_realtime.events SET keywords = keywords + {'" + keyword + "'} WHERE event_name ='" + event_name + "';")
  }

  def deleteKeyword(event_name: String, keyword: String) = {
    session.execute("UPDATE epic_realtime.events SET keywords = keywords - {'" + keyword + "'} WHERE event_name ='" + event_name + "';")
  }

  def getRows: ResultSetFuture = {
    val query = QueryBuilder.select().all().from("epic_realtime", "events")
    session.executeAsync(query)
  }

  def countFrom(table: String): Long = {
    session.execute(s"select count(*) from epic_realtime.$table").one.getLong(0)
  }

  def dropSchema() = {
    session.execute("DROP KEYSPACE epic_realtime")
  }

  def close() {
    session.close
    cluster.close
  }

}

//https://github.com/magro/play2-scala-cassandra-sample/blob/master/app/models/Cassandra.scala
// https://docs.datastax.com/en/developer/java-driver/2.0/common/drivers/reference/cqlStatements.html
// https://docs.datastax.com/en/developer/java-driver/2.0/java-driver/reference/queryBuilderOverview.html
// https://docs.datastax.com/en/developer/java-driver/2.0/java-driver/reference/queryBuilder_r.html
// https://docs.datastax.com/en/drivers/java/2.0/com/datastax/driver/core/Row.html
// https://github.com/UnderstandLingBV/Tuktu/blob/master/modules/nosql/app/tuktu/nosql/util/cassandra.scala
// http://www.programcreek.com/java-api-examples/index.php?api=com.datastax.driver.core.Row
