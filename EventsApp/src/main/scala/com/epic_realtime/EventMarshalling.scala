package com.epic_realtime

import spray.json._

case class EventEntry(description: String, startdate: String )//, enddate: String) 

case class ActiveEntry(active: String) 

case class KeywordsEntry(word: String) 

case class MassageEntry(msg: String) 

case class Error(message: String) 

trait EventMarshalling  extends DefaultJsonProtocol {
  import EventSupervisor._

  implicit val eventEntryFormat = jsonFormat2(EventEntry)
  implicit val eventActiveEntry = jsonFormat1(ActiveEntry)
  implicit val keywordsEntryFormat = jsonFormat1(KeywordsEntry)
  implicit val massageEntryFormat = jsonFormat1(MassageEntry)

  implicit val eventFormat = jsonFormat5(Event)
  implicit val eventsFormat = jsonFormat1(Events)
  implicit val tweetcountFormat = jsonFormat1(EventActor.TweetCount)
  implicit val keywordFormat = jsonFormat1(EventActor.Keyword)
  implicit val keywordsFormat = jsonFormat1(EventActor.Keywords)
  implicit val msgFormat = jsonFormat1(EventActor.Msg)
  implicit val errorFormat = jsonFormat1(Error)
}
