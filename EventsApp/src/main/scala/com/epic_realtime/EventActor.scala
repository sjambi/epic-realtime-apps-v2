package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask

import com.datastax.driver.core.{Session, Cluster, Host, Metadata}
import com.github.nscala_time.time.Imports._

import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._


object EventActor {
  def props(name: String) = Props(new EventActor(name))
 
  case class TweetCount(count: String)
  case class Keyword(text: String)
  case class Keywords(keywords: Vector[Keyword] = Vector.empty[Keyword])
  
  case object SaveEvent 
  case class Description(desc: String = "")
  case class StartDate(sDate: String = "")
  case class EndDate(eDate: String = "")
  case class SetActive(status: Boolean)
  case object GetEvent 
  case object Cancel 
  case class Active(active: Boolean)
  case object ResetTweetCount
  case object GetTweetCount  
  case object GetKeywords
  case class LoadKeywords(keywordset: java.util.Set[String])
  case class AddKeyword(word: String) 
  case class DeleteKeyword(word: String)
  case object CreateKafkaTopic 
  case class KafkaTopic(name: String)
  case class PostMsg(msg: String)
  case class Msg(text: String)
  case object StartProduceTweets
  case object StopProduceTweets

  sealed trait KafkaResponse 
  case class TopicCreated(topic: KafkaTopic) extends KafkaResponse 
  case object TopicFailed extends KafkaResponse 

}


class EventActor(name: String) extends Actor {
  import EventActor._
  import context._

  var description = ""
  var startDate = ""
  var endDate = ""
  var isActive = false
  var keywords = Vector.empty[Keyword] 
  var count = 0L

  def createKafkaProducerActor(name: String) = 
    context.actorOf(KafkaProducerActor.props(name), name) 

  def createCassandraActor(name: String) = 
    context.actorOf(CassandraActor.props(name), name) 

  val kafkaProducer = createKafkaProducerActor(name+"_producer")
  val cassandraWriter = createCassandraActor(name+"_cassandra")

  def receive = {

    case Description(desc) =>
      description = desc
   
    case StartDate(sDate) =>
      startDate = sDate

    case EndDate(eDate) =>
      endDate = eDate

    case SetActive(status) =>
      isActive = status

    case ResetTweetCount =>
      count = 0

    case SaveEvent =>
      val cassandraClient = new CassandraClient()
      cassandraClient.addEvent(name, description, startDate, endDate, isActive)
      cassandraClient.close


    case GetEvent => 
      sender() ! Some(EventSupervisor.Event(name, description, startDate, endDate, isActive)) 


    case Cancel =>
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteEvent(name)
      cassandraClient.close
      sender() ! Some(EventSupervisor.Event(name, description, startDate, endDate, isActive))
      self ! PoisonPill


    case Active(active) =>
      if (active==true && keywords.isEmpty){
        sender() ! Some(EventSupervisor.Event(name, description, startDate, endDate, isActive))
      } else {
        isActive = active
        isActive match {
          case true =>
            // Start collecting and saving tweets 
            self ! StartProduceTweets
          case false =>       
            // Set the endtiem date
            endDate = DateTime.now.toString("MM/dd/yyyy")
            // Stop collecting and saving tweets 
            self ! StopProduceTweets
          case _ => // do nothing //println("Error in setting the event's active status.")
        }
        // Update the new active status
        val cassandraClient = new CassandraClient()
        cassandraClient.setEventActiveStatus(name, isActive, endDate)
        cassandraClient.close
        sender() ! Some(EventSupervisor.Event(name, description, startDate, endDate, isActive))
      } 


    case GetTweetCount =>
      val cassandraClient = new CassandraClient()
      count = cassandraClient.getTweetCount(name)
      cassandraClient.close
      sender() ! TweetCount(count.toString)


    case GetKeywords =>
      sender() ! Keywords(keywords)


    case LoadKeywords(keywordset) => 
      keywords = Vector.empty[Keyword]
      for (word <- keywordset){
        keywords = keywords :+ Keyword(word)
      }


    case AddKeyword(newkeyword) => 
      if (keywords.contains(Keyword(newkeyword)) == false) {
        keywords = keywords :+ Keyword(newkeyword)
        // Update event's keywords list
        val cassandraClient = new CassandraClient()
        cassandraClient.addKeyword(name, newkeyword)
        cassandraClient.close
        // Add the keyword to tweet collection
        if (isActive == true){
          // self ! StopProduceTweets
          self ! StartProduceTweets
        }
      } 
      sender() ! Keyword(newkeyword)
  

    case DeleteKeyword(keyword) => 
      // Update event's keywords list
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteKeyword(name, keyword)
      self ! LoadKeywords(cassandraClient.getKeywords(name).getSet("keywords", classOf[String]))
      cassandraClient.close
      // Delete the keyword from tweet collection
      if (isActive == true){
        // self ! StopProduceTweets
        self ! StartProduceTweets
      }
      sender() ! Keyword(keyword)


    case CreateKafkaTopic =>
       def create() = {            
          kafkaProducer ! KafkaProducerActor.CreateTopic(name)
          sender() ! TopicCreated(KafkaTopic(name))
        }
        context.child(name).fold(create())(_ => sender() ! TopicFailed) 


    case PostMsg(msg) =>
      // Check if keywords is not empty
      // Connect to Twitter Streaming API to collect tweets based on Keywords
      def notFound() = sender() ! None
      def sendMsg(child: ActorRef) = child forward KafkaProducerActor.PostMsg(name, msg)
      context.child(name).fold(notFound())(sendMsg)
    

    case StartProduceTweets =>
      // Start streaming in from Twitter Streaming API into the event's kafka topic
      var keywordTextArray: Array[String] = Array("")
      for (keyword <- keywords){
        keywordTextArray = keywordTextArray :+ keyword.text
      }
      kafkaProducer ! KafkaProducerActor.CollectTweets(name, keywordTextArray)
      cassandraWriter ! CassandraActor.SaveTweets(name)
      // sender() ! Some(EventSupervisor.Event(name, description, startDate, endDate, isActive)) 


    case StopProduceTweets =>
      // Stop producing and persisting tweets
      kafkaProducer ! KafkaProducerActor.StopProducer(name)
      cassandraWriter ! CassandraActor.StopConsumer(name)

  }
}

