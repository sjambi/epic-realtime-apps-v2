package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

class RestApi(system: ActorSystem, timeout: Timeout) extends RestRoutes {
  implicit val requestTimeout = timeout
  implicit def executionContext = system.dispatcher

  def createEventSupervisor = system.actorOf(EventSupervisor.props, EventSupervisor.name)
}

trait RestRoutes extends EventSupervisorApi
    with EventMarshalling {
  import StatusCodes._

  def routes: Route = eventsRoute ~ eventRoute ~ activeRoute ~ countRoute ~ keywordRoute ~ msgRoute

  def eventsRoute =
    pathPrefix("events") {
      pathEndOrSingleSlash {
        get {
          // GET /events
          onSuccess(getEvents()) { events =>
            complete(OK, events)
          }
        }
      }
    }
  
  def eventRoute =
    pathPrefix("events" / Segment) { event =>
      pathEndOrSingleSlash {
        post {
          // POST /events/:event
          entity(as[EventEntry]) { ed => 
            onSuccess(createEvent(event, ed.description, ed.startdate)) { 
              case EventSupervisor.EventCreated(event) => complete(Created, event) 
              case EventSupervisor.EventExists =>
                val err = Error(s"$event event exists already.")
                complete(BadRequest, err) 
            }
          }
        } ~
        get {
          // GET /events/:event
          onSuccess(getEvent(event)) {
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        } ~
        delete {
          // DELETE /events/:event
          onSuccess(cancelEvent(event)) {
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        }
      }
    }

  def activeRoute =
    pathPrefix("events" / Segment / "active") { event =>
      pathEndOrSingleSlash {
        post {
          // POST /events/:event/active
          entity(as[ActiveEntry]) { ae => 
            onSuccess(activeEvent(event, ae.active.toBoolean)) {
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          }
        }
      }
    }

  def countRoute =
    pathPrefix("events" / Segment / "count") { event =>
      pathEndOrSingleSlash {
        get {
          // GET /events/:event/count
          onSuccess(getTweetCount(event)) { count =>
            complete(OK, count)
          }
          // onSuccess(getTweetCount(event)) {
            // _.fold(complete(NotFound))(e => complete(OK, e))
        }
      }
    }

  def keywordRoute =
    pathPrefix("events" / Segment / "keywords") { event =>
      pathEndOrSingleSlash {
        get {
          // GET /events/:event/keywords
          onSuccess(getKeywords(event)) { keywordlist =>
            complete(OK, keywordlist)
          }
        } ~
        post {
          // POST /events/:event/keywords
          entity(as[KeywordsEntry]) { kw => 
            onSuccess(addKeyword(event, kw.word)) { keyword =>
              complete(Created, keyword) 
            }
          }
        } ~
        delete {
          // DELETE /events/:event/keywords
          entity(as[KeywordsEntry]) { kw => 
            onSuccess(deleteKeyword(event, kw.word)) { keyword =>
              complete(OK, keyword) 
            }
          }
        }
      }
    }

  def msgRoute =
    pathPrefix("events" / Segment / "post") { event =>
      pathEndOrSingleSlash {
        post {
          // POST /events/:event/active
          entity(as[MassageEntry]) { me => 
            onSuccess(postMassage(event, me.msg)) { 
              //m => complete(Created, m)
              //_.fold(complete(NotFound))(e => complete(OK, e))
              case KafkaProducerActor.PostSuccess(msg) => complete(Created, msg) 
              case KafkaProducerActor.PostFailure(msg) =>
                val err = Error(msg)
                complete(BadRequest, err) 
            }
          }
        }
      }
    }

}


trait EventSupervisorApi {
  import EventSupervisor._

  def createEventSupervisor(): ActorRef

  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val eventSupervisor = createEventSupervisor()

  def createEvent(name: String, description: String, startDate: String) =
    eventSupervisor.ask(CreateEvent(name, description, startDate, "", false, false))
      .mapTo[EventResponse]

  def activeEvent(name: String, active: Boolean) =
    eventSupervisor.ask(ActiveEvent(name, active))
      // .mapTo[EventResponse]
      .mapTo[Option[Event]]

  def getEvents() =
    eventSupervisor.ask(GetEvents).mapTo[Events]

  def getEvent(name: String) =
    eventSupervisor.ask(GetEvent(name))
      .mapTo[Option[Event]]

  def getTweetCount(name: String) =
    eventSupervisor.ask(GetTweetCount(name))
      .mapTo[EventActor.TweetCount]

  def getKeywords(name: String) =
    eventSupervisor.ask(GetKeywords(name))
      .mapTo[EventActor.Keywords]

 def addKeyword(name: String, word: String) =
    eventSupervisor.ask(AddKeyword(name, word))
      .mapTo[EventActor.Keyword]

  def deleteKeyword(name: String, word: String) =
    eventSupervisor.ask(DeleteKeyword(name, word))
      .mapTo[EventActor.Keyword]

  def cancelEvent(name: String) =
    eventSupervisor.ask(CancelEvent(name))
      .mapTo[Option[Event]]

  def postMassage(name: String, msg: String) =
    eventSupervisor.ask(PostMassage(name, msg))
      .mapTo[KafkaProducerActor.PostResponse]

}

