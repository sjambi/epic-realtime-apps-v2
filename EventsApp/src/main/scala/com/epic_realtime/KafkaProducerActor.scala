package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }
import java.util.Properties
import scala.util.{Failure, Success}
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Await}
import ExecutionContext.Implicits.global
import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer, ProducerConfig}
import com.typesafe.config.{ Config, ConfigFactory }


object KafkaProducerActor {
  def props(name: String) = Props(new KafkaProducerActor(name))
 
  case class CreateTopic(topic: String)
  case class PostMsg(topic: String, msg: String) 
  case class CollectTweets(topic: String, keywords: Array[String])
  case class StopProducer(topic: String)

  sealed trait PostResponse 
  case class PostSuccess(msg: EventActor.Msg) extends PostResponse 
  case class PostFailure(err: String) extends PostResponse

  sealed trait TopicResponse 
  case class TopicSuccess(msg: String) extends TopicResponse 
  case class TopicFailure(err: String) extends TopicResponse

}


class KafkaProducerActor(name: String) extends Actor {
  import KafkaProducerActor._

  val config = ConfigFactory.load() 
  val kafka_server = config.getString("kafka.server")
  val zookeeper = config.getString("kafka.zookeeper")
  val kafka_replication = config.getString("kafka.replication")
  val kafka_partitions = config.getString("kafka.partitions") 
  val kafka_path = config.getString("kafka.path") 

  val pr = new Properties()   
  pr.put("bootstrap.servers", kafka_server)
  pr.put("acks", "all")
  pr.put("retries", "0")
  pr.put("batch.size", "16384")
  pr.put("linger.ms", "0")
  pr.put("buffer.memory", "33554432")
  pr.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  pr.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  //https://kafka.apache.org/08/configuration.html

  //val topic = name
  val producer = new KafkaProducer[String, String](pr)
  import twitter4j.Status
  import twitter4j.json.DataObjectFactory
  import play.api.libs.json._  //Json
  import com.epic_realtime.utils.TwitterClient
  val twitterClient = new TwitterClient()


  def receive = {

    case CreateTopic(topic) =>
      try {
        // Create new Kafka topic using a command-line
        import sys.process._
        kafka_path + "/bin/kafka-topics.sh --create " + 
        "--zookeeper " + zookeeper +
        "--replication-factor " + kafka_replication +
        " --partitions " +  kafka_partitions +
        " --topic " + topic !
        // Note: This command will throw an error if the topic already exists.
        // ERROR kafka.common.TopicExistsException: Topic "topic_name" already exists.

        // Create new Kafka topic using Kafka Admin (Not wroking)
        //.......................................................
        // import kafka.admin.AdminUtils
        // import kafka.utils.ZKStringSerializer
        // import kafka.utils.ZkUtils
        // import org.I0Itec.zkclient.ZkClient
        // import org.I0Itec.zkclient.ZkConnection

        // val zookeeperHosts = "localhost:2181"
        // val sessionTimeoutMs = 10000
        // val connectionTimeoutMs = 10000
        // val zkClient = new ZkClient(zookeeperHosts, sessionTimeoutMs, connectionTimeoutMs, ZKStringSerializer)
        // // Security for Kafka was added in Kafka 0.9.0.0
        // val isSecureKafkaCluster = false
        // val zkUtils = new ZkUtils(zkClient, new ZkConnection(zookeeperHosts), isSecureKafkaCluster)        
        // // ZkUtils.apply("zookeeper1:port1,zookeeper2:port2", sessionTimeoutMs, connectionTimeoutMs, false)
        // val topicName = "test"
        // val numPartitions = 1 //8
        // val replicationFactor = 1 //3
        // val topicConfig = new Properties
        // // AdminUtils.createTopic(zkClient, topicName, numPartitions, replicationFactor, topicConfig)
        // AdminUtils.createTopic(zkUtils, topicName, numPartitions, replicationFactor, topicConfig)
        // zkClient.close()
        // // http://stackoverflow.com/questions/36364872/creating-a-topic-for-apache-kafka-0-9-using-java
        // // http://stackoverflow.com/questions/27006156/how-create-kafka-zkstringserializer-in-java
	
        sender ! TopicSuccess("Kafka creating topic success")
      } catch{
	        case e: Exception => {
	          println(e.printStackTrace())
	          sender ! TopicFailure("Kafka creating topic error")
	        }
      }

  	case PostMsg(topic, msg) =>
      // push the msg to Kafka       
      try{
      	println("A new msg '" + msg + "' is posted!!")
      	
        val future = Future{
          producer.send(new ProducerRecord[String, String](topic, msg))
        }
        val result = Await.result(future, 1 second).get()        
        sender ! PostSuccess(EventActor.Msg(msg))
      } catch{
        case e: Exception => {
          println(e.printStackTrace())
          sender ! PostFailure("Kafka push error")
        }
      }    

    case CollectTweets(topic, keywords) =>      
      def handler(status: Status) = {
        val rawjson = DataObjectFactory.getRawJSON(status)
        //println(rawjson)
        val data = DataObjectFactory.createStatus(rawjson)
        //println("@" + data.getUser().getScreenName() + " - " + data.getText())
        //getUser().getId()
        //getUser().getName()
        ///val tweet = Json.parse(rawjson) \ "text"
        //val tweet = Json.parse(data) //wrong
        ///producer.send(new ProducerRecord[String, String](topic, tweet.toString))
        //producer.send(new ProducerRecord[String, String](topic, data.toString))
        producer.send(new ProducerRecord[String, String](topic, rawjson))
        println(s"${topic}: The producer posted: ${data.getText()}")

      }
      twitterClient.addListener(handler)
      twitterClient.addFilter(keywords)
      //twitterClient.run   

    case StopProducer(topic) =>
      //producer.close()
      twitterClient.stopListener()
      producer.flush()
      println("\n\n\n-----------------------------------------\n\n\n")
      print(s"${topic}: The producer is closed.")
      println("\n\n\n-----------------------------------------\n\n\n")
      //Close this producer.
      //close(long timeout, java.util.concurrent.TimeUnit timeUnit)
      //This method waits up to timeout for the producer to complete the sending of all incomplete requests.
      
  }
}
