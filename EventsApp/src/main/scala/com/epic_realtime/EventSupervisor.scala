package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout

import com.epic_realtime.utils.CassandraClient
// import com.datastax.driver.core.ResultSet
import scala.collection.JavaConversions._

object EventSupervisor {
  def props(implicit timeout: Timeout) = Props(new EventSupervisor)
  def name = "eventSupervisor"

  case object LoadEvents
  case class CreateEvent(name: String, description: String, startDate: String, endDate: String, isActive: Boolean, loaded: Boolean) 
  case class GetEvent(name: String) 
  case class GetTweetCount(name: String)
  case class GetKeywords(name: String) 
  case class LoadKeywords(name: String, keywordset: java.util.Set[String])
  case class AddKeyword(name: String, word: String) 
  case class DeleteKeyword(name: String, word: String)  
  case object GetEvents 
  case class CancelEvent(name: String) 
  case class ActiveEvent(name: String, active: Boolean) 
  case class PostMassage(name: String, msg: String)

  case class Event(name: String, description: String, startDate: String, endDate: String = "", isActive: Boolean = false) 
  case class Events(events: Vector[Event]) 

  sealed trait EventResponse 
  case class EventCreated(event: Event) extends EventResponse 
  case object EventExists extends EventResponse 
  case object EventActivated extends EventResponse 
  case object ErrorActivated extends EventResponse 

  // sealed trait ActivateResponse 
  // case object EventActivated extends ActivateResponse 
  // case object ErrorActivated extends ActivateResponse 
}

class EventSupervisor(implicit timeout: Timeout) extends Actor {
  import EventSupervisor._
  import context._

  def createEventActor(name: String) =
    context.actorOf(EventActor.props(name), name) 

  def intialize_loadEvents():Int = {
    val cassandraClient = new CassandraClient()
    cassandraClient.createSchema
    val resultSet = cassandraClient.getEvents
    // val rows = resultSet.all()
    var count = 0
    for (row <- resultSet) {
      var name = row.getString("event_name")
      var description = row.getString("event_description")
      var startDate = row.getString("start_date")
      var endDate = row.getString("end_date")
      var isActive = row.getBool("is_active")
      var keywords = row.getSet("keywords", classOf[String]) 
      count += 1
      self ! CreateEvent(name, description, startDate, endDate, isActive, true)
      self ! LoadKeywords(name, keywords)
    }
    cassandraClient.close
    return count
  }

  override def preStart() {
    println("\n------------------------------------------------\n")
    print(s"${intialize_loadEvents} EVENT(S) LOADED.")
    println("\n------------------------------------------------\n")
  }

  def receive = {

    // case LoadEvents =>
    //   println(s"${intialize_loadEvents} EVENT(S) LOADED.")

    case CreateEvent(name, description, startDate, endDate, isActive, loaded) =>
      def create() = {  
        val eventActor = createEventActor(name)
        eventActor ! EventActor.Description(description)
        eventActor ! EventActor.StartDate(startDate)
        eventActor ! EventActor.EndDate(endDate)
        eventActor ! EventActor.SetActive(isActive)
        if (!loaded) eventActor ! EventActor.SaveEvent
        //eventActor ! EventActor.CreateKafkaTopic
        //eventActor ! EventActor.PostMsg("Hello")  
        sender() ! EventCreated(Event(name, description, startDate, endDate, isActive))
      }
      context.child(name).fold(create())(_ => sender() ! EventExists) 


    case GetEvent(name) =>
      def notFound() = sender() ! None
      def getEvent(child: ActorRef) = child forward EventActor.GetEvent
      context.child(name).fold(notFound())(getEvent)


    case GetTweetCount(name) =>
      def notFound() = sender() ! EventActor.TweetCount
      def getTweetCount(child: ActorRef) = child.forward(EventActor.GetTweetCount)
      context.child(name).fold(notFound())(getTweetCount) 


    case GetKeywords(name) =>
      def notFound() = sender() ! EventActor.Keywords() //None?
      def getKeywords(child: ActorRef) = child.forward(EventActor.GetKeywords)
      context.child(name).fold(notFound())(getKeywords) 


    case LoadKeywords(name, keywordset) => 
      def notFound() = sender() ! EventActor.Keywords() //None?
      def loadKeywords(child: ActorRef) = child.forward(EventActor.LoadKeywords(keywordset))
      context.child(name).fold(notFound())(loadKeywords) 


    case AddKeyword(name, keyword) =>
      def notFound() = sender() ! None
      def addKeyword(child: ActorRef) = child.forward(EventActor.AddKeyword(keyword))
      context.child(name).fold(notFound())(addKeyword) 

    
    case DeleteKeyword(name, keyword) =>
      def notFound() = sender() ! None
      def deleteKeyword(child: ActorRef) = child.forward(EventActor.DeleteKeyword(keyword))
      context.child(name).fold(notFound())(deleteKeyword) 

    
    case GetEvents =>
      import akka.pattern.ask
      import akka.pattern.pipe
      def getEvents = context.children.map { child =>
        self.ask(GetEvent(child.path.name)).mapTo[Option[Event]] 
      }
      def convertToEvents(f: Future[Iterable[Option[Event]]]) =
        f.map(_.flatten).map(l=> Events(l.toVector)) 
      pipe(convertToEvents(Future.sequence(getEvents))) to sender() 
    

    case CancelEvent(name) =>
      def notFound() = sender() ! None
      def cancelEvent(child: ActorRef) = child forward EventActor.Cancel
      context.child(name).fold(notFound())(cancelEvent)


    case ActiveEvent(name, active) =>
      def notFound() = sender() ! None
      def activeEvent(child: ActorRef) = child forward EventActor.Active(active)
      context.child(name).fold(notFound())(activeEvent)
   

    case PostMassage(name, msg) =>
      def notFound() = sender() ! None
      def postMassage(child: ActorRef) = child forward EventActor.PostMsg(msg)
      context.child(name).fold(notFound())(postMassage)

  }
}

