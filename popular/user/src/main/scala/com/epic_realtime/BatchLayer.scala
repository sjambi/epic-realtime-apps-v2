package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import play.api.libs.json._ 
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import com.github.nscala_time.time.Imports._


object BatchLayer {

  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: com.epic_realtime.BatchLayer <cores> <event_name> <query_name> " +
        "<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }

    val cores = args(0)
    val event = args(1)
    val query_name = args(2)
    val trend = if (args.length > 3) args(3) else "topk"   //default value
    val value = if (args.length > 4) args(4) else "10"     //default value
    val start_date = if (args.length > 5) args(5) else "" 
    val end_date = if (args.length > 6) args(6) else "" 

    val conf = SparkCommon.conf(s"${query_name}_batch", cores)
    val sc = SparkCommon.sc(conf)
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    // Query Date Entry:
    // - Now           =>   start_date = "",          end_date = ""
    // - Since         =>   start_date = user_input,  end_date = ""
    // - Time Period   =>   start_date = user_input,  end_date = user_input

    if (start_date.isEmpty){
      println("No Batch Job Required.")
      producer.send(new ProducerRecord[String, String] (topic, "No Batch Job Required."))
      // Clean up any old batch view 
      CassandraConnector(conf).withSessionDo { session =>
        session.execute(s"TRUNCATE epic_realtime.${query_name.toLowerCase()}_batch")
      }
    } else {

      // Start up a batch job
      //.................................................................................

      var filter = ""
      if (end_date.isEmpty){
        //Since
        filter = s"event_name = '${event}' and created_at >= '${start_date} 00:00:00+0200'"
      } else {
        //Time Period
        filter = s"event_name = '${event}' and created_at >= '${start_date} 00:00:00+0200' and created_at <= '${end_date} 23:59:00+0200'"
      }

      var count = 0
      while (true){

        val d1 = DateTime.now

        // Get tweets filtered by a date range
        val tweetRDD = sc.cassandraTable("epic_realtime", "tweets")
          .select("tweet_json")
          .where(filter)

        println(s"TWEET COUNT = ${tweetRDD.count}")

        // Extract tweet values
  
        val retweets = tweetRDD.map{ row => 
          (Json.parse(row.getString("tweet_json")) \ "retweeted_status") match {         
            case _: JsUndefined => 
              (
              "", 
              0L, 
              "")
            case _ => 
              (
              (Json.parse(row.getString("tweet_json")) \ "retweeted_status" \ "id_str").toString.replaceAll("\"", ""),
              (Json.parse(row.getString("tweet_json")) \ "retweeted_status" \ "retweet_count").as[Int],
              (Json.parse(row.getString("tweet_json")) \ "retweeted_status" \ "user" \ "screen_name").toString.replaceAll("\"", "")
              )
          }//match
        }//map
        // .collect.foreach(println)


        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._

        if (end_date.nonEmpty){ // No speed job required, only batch job
          // Save results as a batch view
          //.............................           
          val retweetCount = retweets
            .toDF.select(col("_1").alias("tweet_id"),col("_2").alias("retweet_count"),col("_3").alias("user"))
            .filter(col("retweet_count")>0) //no retweet found
            .groupBy("user","tweet_id")
            .agg(max("retweet_count"))
            .select(col("user"),col("max(retweet_count)"))
            .groupBy("user")
            .agg(sum("max(retweet_count)").alias("count"))
            .write
              .format("org.apache.spark.sql.cassandra")
              .options(Map( "table" -> s"${query_name.toLowerCase()}_batch", "keyspace" -> "epic_realtime"))
              .mode(SaveMode.Overwrite)
              .save()
          println("SAVE TO BATCH FILE")
  
        } else {

          // Combine batch results with realtime results
          //............................................. 
          // Save results as a temp view
          // val retweetCount = retweets
            // .toDF.select(col("_1").alias("tweet_id"),col("_2").alias("retweet_count"),col("_3").alias("user"),col("_4").alias("tweet_text"))
            // .filter(col("retweet_count")>0) //no retweet found
            // .groupBy("user","tweet_id","tweet_text")
            // .agg(max("retweet_count"))
            // .show
            // .select(col("user"),col("max(retweet_count)"))
            // .groupBy("user")
            // .agg(sum("max(retweet_count)").alias("count"))
            // .write
            //   .format("org.apache.spark.sql.cassandra")
            //   .options(Map( "table" -> s"${query_name.toLowerCase()}_tmp2", "keyspace" -> "epic_realtime"))
            //   .mode(SaveMode.Overwrite)
            //   .save()
          println("SAVE TO BATCH TMP2 FILE")

          // // Combine realtime and temp views
          // val speed_result = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_realtime")
          // val batch_result = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_tmp2")         
          // val union_result = speed_result.union(batch_result)
          // union_result.map(row => (row.getString("user"), row.getInt("count")))
          //   .toDF.select(col("_1").alias("user"),col("_2").alias("count"))
          //   .groupBy("user")
          //   .agg(max("count").alias("count"))
          //   .write              
          //    .format("org.apache.spark.sql.cassandra")
          //   .options(Map( "table" -> s"${query_name.toLowerCase()}_batch", "keyspace" -> "epic_realtime"))
          //   .mode(SaveMode.Overwrite)
          //   .save()

          println("SAVE TO BATCH FILE FROM REALTIME AND TMP2")

          // Clean up the realtime view
          CassandraConnector(conf).withSessionDo { session =>
            session.execute(s"TRUNCATE epic_realtime.${query_name.toLowerCase()}_realtime")
          }
        }//if-else-end

        val d2 = DateTime.now
        val processingTime = (d2.getMillis()-d1.getMillis()).toFloat/1000

        count += 1
        println("\n\n\n============================================\n")
        println(s"WHILE LOOP: COUNT = ${count}")
        println("\n============================================\n\n\n")

        producer.send(new ProducerRecord[String, String]
          (topic, s"Batch (${count}): [${start_date} - ${end_date}], ${tweetRDD.count}, ${processingTime}"))

        // Call Serving Job
        // ServingLayer_tmp.merge(conf, sc, query_name, "batch", trend, value)


      }//while-loop-end
      //sc.stop()
    }//batch-job-end

  }
}
