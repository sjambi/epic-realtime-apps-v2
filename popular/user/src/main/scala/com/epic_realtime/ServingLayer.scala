package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._


object ServingLayer {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: com.epic_realtime.ServingLayer <cores> <seconds> <event_name> <query_name> " +
        "<trendtype[threshold/topk] <trendvalue>")
      System.exit(1)
    }
    
    val cores = args(0)
    val seconds = args(1)
    val event = args(2)
    val query_name = args(3)
    val trend = if (args.length > 4) args(4) else "topk"   //default value
    val value = if (args.length > 5) args(5) else "10"     //default value
 
    val conf = SparkCommon.conf(query_name, cores)
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, seconds.toInt)

    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    // Combine data from realtime and batch views using union
    val realtimeRDD = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_realtime")
    val batchRDD = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_batch")
    val unionRDD = realtimeRDD.union(batchRDD)
    // unionRDD.cache()
    
    println(s"REALTIME COUNT = ${realtimeRDD.count}")
    println(s"BATCH COUNT = ${batchRDD.count}")

    // Generate continuous stream of Dstream
    val dstream = new ConstantInputDStream(ssc, unionRDD)

    dstream.foreachRDD{ rdd =>

      // if (rdd.count>0){

        if (realtimeRDD.count>0 || batchRDD.count>0){

        println("\n\n\n============================================\n")
        println(s"RDD COUNT = ${rdd.count}")
        println("\n============================================\n\n\n")

        val d1 = DateTime.now

        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._

        val userCount = rdd.map(row => (row.getString("user"), row.getInt("count")))
            .toDF.select(col("_1").alias("user"),col("_2").alias("count"))
            .groupBy("user")
            .agg(max("count").alias("count"))
    
        val popularUser =
          trend match {
            case "threshold" => userCount.where("count>="+value)
            case "topk" => userCount.limit(value.toInt)
            case _ => userCount.limit(10)
          }      

        // Save results as a query result view
        popularUser
        .write
          .format("org.apache.spark.sql.cassandra")
          .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
          .mode(SaveMode.Overwrite)
          .save()
       
        val d2 = DateTime.now
        val processingTime = (d2.getMillis()-d1.getMillis()).toFloat/1000
           
        val data = s"Serving: ${realtimeRDD.count}, ${batchRDD.count}, ${rdd.count}, ${popularUser.count}, ${processingTime}"
        producer.send(new ProducerRecord[String, String] (topic, data))

      }//rdd.count
      else{
        println("\n\n\n============================================\n")        
        println("NO RDD FOUND")
        println("\n============================================\n\n\n")
      }
    }

    ssc.start()
    ssc.awaitTermination()
    // ssc.stop()

  }
}

