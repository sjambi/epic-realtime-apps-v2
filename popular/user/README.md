retweet
=======
This app contains three types of Spark jobs that create a Lambda Architecture process for Popular Queries that flexibly find popular tweets or popular users in Twitter streaming and historical data of a requested event.


************** Spark Jobs ***************


SpeedLayer
----------
Creates a retweet real-time view for the requested event. It consumes Twitter data streaming from event's Kafka topic, reads retweeted_status (if found) from tweet json object along with tweet's id and created_at values, then stores a tuple of (event_name, tweet_id, time, retweet_id, retweet_count, retweet_user_id, retweet_screen_name, retweet_text) into a Cassandra table called "retweets_realtime".

com.epic_realtime.SpeedLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.SpeedLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



BatchLayer
----------
Creates a retweet batch view for the requested event. It retrieves Twitter historical data stored in a Cassandra table called "tweets" that contains all previously collected tweet raw data for all events. It filters in event's tweets, reads tweet_id and created_at columns, reads retweeted_status (if found) from the tweet_json column, then stores a tuple of (event_name, tweet_id, time, retweet_id, retweet_count, retweet_user_id, retweet_screen_name, retweet_text) into a Cassandra table called "retweets_batch".

com.epic_realtime.BatchLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.BatchLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



TweetServingLayer
-----------------
Creates a query result of realtime and batch aggregates of retweeted tweets for the requested event. It retrieves real-time and batch retweet data stored in Cassandra tables "retweets_realtime" and "retweets_batch", merges them, filters data based on a requested time period, then generates a list of popular retweeted tweet id with their counts, either by a threshold that determines if a retweeted tweet is considered popular or a topk value of the most retweeted tweets.
The output is saved in a new Cassandra table named by the query's generated name as tuples of (retweet_id, count, retweet_text), updated every new data streams in. 

com.epic_realtime.TweetServingLayer <event_name> <query_name> <start_date[yyyy-MM-dd]> 
<end_date[yyyy-MM-dd]> <trendtype[threshold/topk] <trendvalue>

Note: 
- if trendtype and trendvalue are not provided, the default values are topk 10
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.TweetServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_tweet_01 2016-07-02 2016-07-03 threshold 10



UserServingLayer
----------------
Creates a query result of realtime and batch aggregates of retweeted users for the requested event. It retrieves real-time and batch retweet data stored in Cassandra tables "retweets_realtime" and "retweets_batch", merges them, filters data based on a requested time period, then generates a list of popular retweeted user id/screen_name with their counts, either by a threshold that determines if a retweeted user is considered popular or a topk value of the most retweeted users.
The output is saved in a new Cassandra table named by the query's generated name as tuples of (retweet_user_id, count, retweet_screen_name), updated every new data streams in. 

com.epic_realtime.UserServingLayer <event_name> <query_name> <start_date[yyyy-MM-dd]> 
<end_date[yyyy-MM-dd]> <trendtype[threshold/topk] <trendvalue>

Note: 
- if trendtype and trendvalue are not provided, the default values are topk 10
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.UserServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_user_01 2016-07-02 2016-07-03 threshold 10


