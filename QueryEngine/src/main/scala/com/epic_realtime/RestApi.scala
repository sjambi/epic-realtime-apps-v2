package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

import com.epic_realtime.utils.CassandraClient

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._



class RestApi(system: ActorSystem, timeout: Timeout) extends RestRoutes {
  implicit val requestTimeout = timeout
  implicit def executionContext = system.dispatcher

  def createQuerySupervisor = system.actorOf(QuerySupervisor.props, QuerySupervisor.name) 
  def selectQueryActor(event: String, querytype: String) =
    system.actorSelection(s"/user/querySupervisor/${event}_${querytype}")//.resolveOne()
  def selectLambdaSupervisor(event: String, querytype: String, lambdatype: String) =
    system.actorSelection(s"/user/querySupervisor/${event}_${querytype}/${event}_${querytype}_${lambdatype}")//.resolveOne()
}


trait RestRoutes extends QuerySupervisorApi
    with LambdaMarshalling {
  import StatusCodes._

  def routes: Route = 
  querySupervisorsRoute ~ querySupervisorRoute ~ queryActorsRoute ~ queryActorRoute ~ 
  lambdaSupervisorsRoute ~ lambdaSupervisorPostRoute ~ lambdaSupervisorGetDelRoute ~ lambdaSupervisorStartRoute ~
  lambdaSupervisorStopRoute ~ lambdaSupervisorQetResultRoute


  // QuerySupervisor Routes ==========================================================================

  def querySupervisorsRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("querytype" / "all") {
        pathEndOrSingleSlash {
          get {
            // GET /events/:event/querytypes
            onSuccess(getQueryActors(event)) { querytypes =>
              complete(OK, querytypes)
            }
          }
        }
      }
    }//event

  def querySupervisorRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>
        pathEndOrSingleSlash {
          val queryActor_name = s"${event}_${querytype}"
          post {
            // POST /events/:event/:querytype
            onSuccess(createQueryActor(queryActor_name, event, querytype)) { 
              case QuerySupervisor.QueryTypeCreated(querytype) => complete(Created, querytype) 
              case QuerySupervisor.QueryTypeExists =>
                  val err = Error(s"Similar $querytype query type exists already.")
                  complete(BadRequest, err) 
              case QuerySupervisor.QueryTypeNoEventError =>
                  val err = Error(s"Event ${event} is not found.")
                  complete(BadRequest, err) 
            }
          } ~
          get {
            // GET /events/:event/:querytype
            onSuccess(getQueryActor(queryActor_name)) {
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          } ~
          delete {
            // DELETE /events/:event/:querytype
            onSuccess(cancelQueryActor(queryActor_name)) {
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          }
        }
      }//querytype
    }//event


  // QueryActor Routes ====================================================================

  def queryActorsRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment / "lambdatype" / "all") { querytype =>
        pathEndOrSingleSlash {
          get {
            // GET /events/:event/:querytype/lambdatypes
            onSuccess(getLambdaSupervisors(event, querytype)) { lambdatypes =>
              complete(OK, lambdatypes)
            }
          }
        }
      }//querytype
    }//event

  def queryActorRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>
        pathPrefix(Segment) { lambdatype =>      
          pathEndOrSingleSlash {
            val lambdaSupervisor_name = s"${event}_${querytype}_${lambdatype}"
            post {
              // POST /events/:event/:querytype/:lambdatype
              onSuccess(createLambdaSupervisor(lambdaSupervisor_name, event, querytype, lambdatype)) { 
                case QueryActor.LambdaTypeCreated(lambdatype) => complete(Created, lambdatype) 
                case QueryActor.LambdaTypeExists =>
                    val err = Error(s"Similar $querytype $lambdatype lambda type exists already.")
                    complete(BadRequest, err) 
                case QueryActor.LambdaTypeNoEventError =>
                    val err = Error(s"Event ${event} is not found.")
                    complete(BadRequest, err) 
              }
            } ~
            get {
              // GET /events/:event/:querytype/:lambdatype
              onSuccess(getLambdaSupervisor(lambdaSupervisor_name, event, querytype)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            } ~
            delete {
              // DELETE /events/:event/:querytype/:lambdatype
              onSuccess(cancelLambdaSupervisor(lambdaSupervisor_name, event, querytype)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }//lambdatype
      }//querytype
    }//event


  // LambdaSupervisor Routes ========================================================================

  def lambdaSupervisorsRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>
        pathPrefix(Segment / "query" / "all") { lambdatype =>
          pathEndOrSingleSlash {
            get {
              // GET /events/:event/:querytype/:lambdatype/query/all
              onSuccess(getLambdaActors(event, querytype, lambdatype)) { queries =>
                complete(OK, queries)
              }
            }
          }
        }//lambdatype
      }//querytype
    }//event

  def lambdaSupervisorPostRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>   
        pathPrefix(Segment / "query") { lambdatype =>   
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/:querytype/:lambdatype/query
              entity(as[LambdaQueryDescription]) { td => 
                onSuccess(createLambdaActor(event, querytype, lambdatype, td.lowertime, td.uppertime, td.trendtype, td.value)) { 
                  case LambdaSupervisor.LambdaQueryCreated(lambda) => complete(Created, lambda) 
                  case LambdaSupervisor.LambdaQueryExists =>
                      val err = Error(s"Similar $querytype $lambdatype query exists already.")
                      complete(BadRequest, err) 
                }
              }
            }
          }
        }//lambdatype
      }//querytype
    }//event

  def lambdaSupervisorGetDelRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>   
        pathPrefix(Segment / "query") { lambdatype =>           
          pathPrefix(Segment ) { name =>
            pathEndOrSingleSlash {
              get {
                 // GET /events/:event/:querytype/:lambdatype/query/:name
                onSuccess(getLambdaActor(name, event, querytype, lambdatype)) {
                  _.fold(complete(NotFound))(e => complete(OK, e))
                }
              } ~
              delete {
                 // GET /events/:event/:querytype/:lambdatype/query/:name
                onSuccess(cancelLambdaActor(name, event, querytype, lambdatype)) {
                  _.fold(complete(NotFound))(e => complete(OK, e))
                }
              }
            }
          }//name
        }//lambdatype
      }//querytype
    }//event

  def lambdaSupervisorStartRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>   
        pathPrefix(Segment / "query") { lambdatype =>      
          pathPrefix(Segment / "start") { name =>
            pathEndOrSingleSlash {
              post {
                // POST /events/:event/:querytype/:lambdatype/query/:name/start
                onSuccess(startLambdaActor(name, event, querytype, lambdatype)) { 
                  _.fold(complete(NotFound))(e => complete(OK, e))
                }
              }
            }
          }//name
        }//lambdatype
      }//querytype
    }//event

  def lambdaSupervisorStopRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>         
        pathPrefix(Segment / "query") { lambdatype =>      
          pathPrefix(Segment / "stop") { name =>
            pathEndOrSingleSlash {
              post {
                // POST /events/:event/:querytype/:lambdatype/query/:name/stop
                onSuccess(stopLambdaActor(name, event, querytype, lambdatype)) { 
                  _.fold(complete(NotFound))(e => complete(OK, e))
                }
              }
            }
          }//name
        }//lambdatype
      }//querytype
    }//event
    
  def lambdaSupervisorQetResultRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix(Segment) { querytype =>         
        pathPrefix(Segment / "query") { lambdatype =>      
          pathPrefix(Segment / "result") { name =>
            pathEndOrSingleSlash {
              get {
                // GET /events/:event/:querytype/:lambdatype/query/:name/result
                onSuccess(getLambdaActorResult(name, event, querytype, lambdatype)) { result =>
                  complete(OK, result)
                }
              }
            }
          }//name
        }//lambdatype
      }//querytype
    }//event

}


trait QuerySupervisorApi {
  import QuerySupervisor._
  import QueryActor._
  import LambdaSupervisor._
  import LambdaActor._

  def createQuerySupervisor(): ActorRef
  def selectQueryActor(event: String, querytype: String): ActorSelection
  def selectLambdaSupervisor(event: String, querytype: String, lambdatype: String): ActorSelection

  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val querySupervisor = createQuerySupervisor()
  println(s"\nQuerySupervisor is created with Actor Reference ${querySupervisor}.\n")



  // QuerySupervisor Functions ==============================================================================

  def getQueryActors(event: String) = {
    querySupervisor.ask(GetQueryActors(event))
    .mapTo[QueryTypes]
  }

  def createQueryActor(name: String, event: String, querytype: String) = {
    querySupervisor.ask(CreateQueryActor(name, event, querytype, false))
    .mapTo[QueryTypeResponse]
  }

  def getQueryActor(name: String) = {
    querySupervisor.ask(GetQueryActor(name))
    .mapTo[Option[QueryType]]
  }

  def cancelQueryActor(name: String) = {
    querySupervisor.ask(CancelQueryActor(name))
    .mapTo[Option[QueryType]]
  }


  // QueryActor Functions ========================================================================

  def getLambdaSupervisors(event: String, querytype: String) = {
    val queryActor = selectQueryActor(event, querytype)
    println(s"\n${event}: Calling QueryActor.GetLambdaSupervisors by ${queryActor}")   
    queryActor.ask(GetLambdaSupervisors(event, querytype))
    .mapTo[LambdaTypes]
  }

  def createLambdaSupervisor(name: String, event: String, querytype: String, lambdatype: String) = {
    val queryActor = selectQueryActor(event, querytype)
    println(s"\n${event}: Calling QueryActor.CreateLambdaSupervisor by ${queryActor}")   
    queryActor.ask(CreateLambdaSupervisor(name, event, querytype, lambdatype, false))
    .mapTo[LambdaTypeResponse]
  }

  def getLambdaSupervisor(name:String, event: String, querytype: String) = {
    val queryActor = selectQueryActor(event, querytype)
    println(s"\n${event}: Calling QueryActor.GetLambdaSupervisor by ${queryActor}")   
    queryActor.ask(GetLambdaSupervisor(name))
    .mapTo[Option[LambdaType]]
  }

  def cancelLambdaSupervisor(name:String, event: String, querytype: String) = {
    val queryActor = selectQueryActor(event, querytype)
    println(s"\n${event}: Calling QueryActor.CancelLambdaSupervisor by ${queryActor}")   
    queryActor.ask(CancelLambdaSupervisor(name))
    .mapTo[Option[LambdaType]]
  }


  // LambdaSupervisor Functions =============================================================================

  def getLambdaActors(event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.GetLambdaActors by ${lambdaSupervisor}")
    lambdaSupervisor.ask(GetLambdaActors(event, querytype, lambdatype))
    .mapTo[LambdaQueries]
  }

 def createLambdaActor(event: String, querytype: String, lambdatype: String, lowertime: String, uppertime: String, 
    trendtype: String, value: String, running: Boolean=false, 
    speed_job_id: String="", batch_job_id: String="", serving_job_id: String="") = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.CreateLambdaActor by ${lambdaSupervisor}")
    //Create a name for the new query
    val responseFuture = lambdaSupervisor ? GetLambdaActors(event, querytype, lambdatype)
    val result = Await.result(responseFuture, 10 seconds).asInstanceOf[LambdaQueries]
    val query_num = result.query_num
    println(s"query_num = ${query_num}")
    val id = "%02d".format(query_num+1).toString
    val name = s"${event}_${querytype}_${lambdatype}_${id}"
    lambdaSupervisor.ask(CreateLambdaActor(name, event, querytype, lambdatype, lowertime, uppertime, trendtype, value, 
      running, speed_job_id, batch_job_id, serving_job_id, false))
      .mapTo[LambdaQueryResponse]
  }

  def getLambdaActor(name: String, event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.GetLambdaActor by ${lambdaSupervisor}")    
    lambdaSupervisor.ask(GetLambdaActor(name))
      .mapTo[Option[LambdaQuery]]
  }

  def cancelLambdaActor(name: String, event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.CancelLambdaActor by ${lambdaSupervisor}")
    lambdaSupervisor.ask(CancelLambdaActor(name))
      .mapTo[Option[LambdaQuery]]
  }

  def startLambdaActor(name: String, event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.StartLambdaActor by ${lambdaSupervisor}")
    lambdaSupervisor.ask(StartLambdaActor(name))
      .mapTo[Option[LambdaQuery]]
  }

  def stopLambdaActor(name: String, event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.StopLambdaActor by ${lambdaSupervisor}")
    lambdaSupervisor.ask(StopLambdaActor(name))
      .mapTo[Option[LambdaQuery]]
  }

  def getLambdaActorResult(name: String, event: String, querytype: String, lambdatype: String) = {
    val lambdaSupervisor = selectLambdaSupervisor(event, querytype, lambdatype)
    println(s"\n${event}: Calling LambdaSupervisor.GetLambdaActorResult by ${lambdaSupervisor}")
    lambdaSupervisor.ask(GetLambdaActorResult(name))
    querytype match {
      case "dataset" =>
        .mapTo[Vector[LambdaActor.DatasetQueryResult]]
      case "trend" =>
        .mapTo[Vector[LambdaActor.TrendQueryResult]]
      case "popular" => 
        lambdatype match{
          case "tweet" =>
            .mapTo[Vector[LambdaActor.PopularTweetQueryResult]] 
          case "user" =>
            .mapTo[Vector[LambdaActor.PopularUserQueryResult]] 
        }
    }
  }

}

