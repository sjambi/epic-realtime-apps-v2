package com.epic_realtime

import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask

import com.epic_realtime.utils.CassandraClient
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

object LambdaActor {
  def props(name: String) = Props(new LambdaActor(name))
 
  case class Event(name: String = "")
  case class Querytype(q_type: String = "")
  case class Lambdatype(attr: String = "")
  case class Lowertime(time: String = "")
  case class Uppertime(time: String = "")
  case class Trendtype(t: String = "")
  case class Value(v: String = "")
  case class Running(s: Boolean)
  case class Speedjobid(sp_job: String) 
  case class Batchjobid(bt_job: String)
  case class Servingjobid(sr_job: String)
  case object SaveQuery 
  case object GetQuery  
  case object CancelQuery  
  case object StartQuery 
  case object StopQuery
  case object GetQueryResult

  case class DatasetQueryResult(value: String)
  case class TrendQueryResult(value: String, count: Int)
  case class PopularTweetQueryResult(tweet_id: String, count: Int, user: String, tweet_text: String)
  case class PopularUserQueryResult(user: String, count: Int)

}

class LambdaActor(name: String) extends Actor {
  import LambdaActor._
  import context._

  var event = ""
  var querytype = ""
  var lambdatype = ""
  var lowertime = ""
  var uppertime = ""
  var trendtype = ""
  var value = ""
  var running = false
  var speed_job_id = ""
  var batch_job_id = ""
  var serving_job_id = ""


  def createSpeedActor(name: String) = 
    context.actorOf(SpeedActor.props(name), name) 

  def createBatchActor(name: String) = 
    context.actorOf(BatchActor.props(name), name) 

  def createServingActor(name: String) = 
    context.actorOf(ServingActor.props(name), name) 

  val speedActor = createSpeedActor(s"${name}_speed")
  val batchActor = createBatchActor(s"${name}_batch")
  val servingActor = createServingActor(s"${name}_serving")

  
  def receive = {

    case Event(e) =>  event = e

    case Querytype(q_type) => querytype = q_type

    case Lambdatype(attr) =>  lambdatype = attr

    case Lowertime(ltime) =>  lowertime = ltime

    case Uppertime(utime) =>  uppertime = utime

    case Trendtype(t) =>  trendtype = t

    case Value(v) => value = v

    case Running(s) =>  running = s

    case Speedjobid(sp_job) => speed_job_id = sp_job

    case Batchjobid(bt_job) => batch_job_id = bt_job      

    case Servingjobid(sr_job) => serving_job_id = sr_job


    case SaveQuery =>
      println(s"${event}: query $name is saved.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addQuery(name, event, querytype, lambdatype, 
        lowertime, uppertime, trendtype, value, running, speed_job_id, batch_job_id, serving_job_id)
      cassandraClient.close


    case GetQuery => 
      val cassandraClient = new CassandraClient()
      speed_job_id = cassandraClient.getQuerySpeedJobId(name)
      batch_job_id = cassandraClient.getQueryBatchJobId(name)
      serving_job_id = cassandraClient.getQueryServingJobId(name)
      cassandraClient.close       
      sender() ! Some(LambdaSupervisor.LambdaQuery(name, event, querytype, lambdatype,
        lowertime, uppertime, trendtype, value, running, speed_job_id, batch_job_id, serving_job_id))


    case CancelQuery =>
      println(s"${event}: query $name is deleted.")
      // self ! Stop //a problem of getting the spark job id after the recored has been deleted
      speedActor ! SpeedActor.Cancel
      batchActor ! BatchActor.Cancel
      servingActor ! ServingActor.Cancel
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteQuery(event, querytype, lambdatype, name)
      cassandraClient.close
      sender() ! Some(LambdaSupervisor.LambdaQuery(name, event, querytype, lambdatype, 
        lowertime, uppertime, trendtype, value, running, speed_job_id, batch_job_id, serving_job_id))
      self ! PoisonPill
 

    case StartQuery =>
      println(s"${event}: query $name is started.")
      running = true
      speedActor ! SpeedActor.Start(name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime)
      batchActor ! BatchActor.Start(name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime)
      // servingActor ! ServingActor.Start(name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime)
      val cassandraClient = new CassandraClient()
      cassandraClient.setQueryStatus(event, querytype, lambdatype, name, running)
      speed_job_id = cassandraClient.getQuerySpeedJobId(name)
      batch_job_id = cassandraClient.getQueryBatchJobId(name)
      serving_job_id = cassandraClient.getQueryServingJobId(name)     
      cassandraClient.close      
      sender() ! Some(LambdaSupervisor.LambdaQuery(name, event, querytype, lambdatype, 
        lowertime, uppertime, trendtype, value, running, speed_job_id, batch_job_id, serving_job_id))


    case StopQuery =>
      println(s"${event}: query $name is stopped.")
      running = false
      speed_job_id = ""
      batch_job_id = ""
      serving_job_id = ""
      speedActor ! SpeedActor.Stop(name, event, querytype, lambdatype)
      batchActor ! BatchActor.Stop(name, event, querytype, lambdatype)
      // servingActor ! ServingActor.Stop(name, event, querytype, lambdatype)  
      val cassandraClient = new CassandraClient()
      cassandraClient.setQueryStatus(event, querytype, lambdatype, name, running)
      cassandraClient.close  
      sender() ! Some(LambdaSupervisor.LambdaQuery(name, event, querytype, lambdatype, 
        lowertime, uppertime, trendtype, value, running, speed_job_id, batch_job_id, serving_job_id))


    case GetQueryResult =>
      def notFound() = sender() ! None
      def getTrendQueryResult(child: ActorRef) = child forward ServingActor.GetResult(name, event, querytype, lambdatype)
      context.child(s"${name}_serving").fold(notFound())(getTrendQueryResult)


  }
}

