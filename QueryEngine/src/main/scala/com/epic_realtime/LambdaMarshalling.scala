package com.epic_realtime

import spray.json._

case class LambdaQueryDescription(trendtype: String ="", value: String ="", lowertime: String="", uppertime: String="") 

case class MassageEntry(msg: String) 

case class Error(message: String) 


trait LambdaMarshalling extends DefaultJsonProtocol {
  import QuerySupervisor._
  import QueryActor._
  import LambdaSupervisor._
  import LambdaActor._
  
  implicit val queryTypeFormat = jsonFormat2(QuerySupervisor.QueryType)
  implicit val queryTypesFormat = jsonFormat1(QuerySupervisor.QueryTypes)

  implicit val lambdaTypeFormat = jsonFormat3(QueryActor.LambdaType)
  implicit val lambdaTypesFormat = jsonFormat1(QueryActor.LambdaTypes)
 
  implicit val lambdaQueryFormat = jsonFormat12(LambdaSupervisor.LambdaQuery)
  implicit val lambdaQueriesFormat = jsonFormat1(LambdaSupervisor.LambdaQueries)
  
  implicit val datasetQueryResultFormat = jsonFormat1(LambdaActor.DatasetQueryResult)
  implicit val trendQueryResultFormat = jsonFormat2(LambdaActor.TrendQueryResult)
  implicit val popularTweetQueryResultFormat = jsonFormat4(LambdaActor.PopularTweetQueryResult)
  implicit val popularUserQueryResultFormat = jsonFormat2(LambdaActor.PopularUserQueryResult)

  implicit val lambdaQueryDescriptionFormat = jsonFormat4(LambdaQueryDescription)

  implicit val massageEntryFormat = jsonFormat1(MassageEntry)
  implicit val errorFormat = jsonFormat1(Error)

 }
