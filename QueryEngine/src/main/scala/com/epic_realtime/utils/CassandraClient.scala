package com.epic_realtime.utils

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Session
import com.datastax.driver.core.Row
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.ResultSetFuture
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.querybuilder.QueryBuilder._
import com.datastax.driver.core.Metadata
import scala.collection.JavaConversions._
import com.typesafe.config.{ Config, ConfigFactory } 

/**
 * Simple cassandra client, following the datastax documentation
 * (http://www.datastax.com/documentation/developer/java-driver/2.0/java-driver/quick_start/qsSimpleClientCreate_t.html).
 */
class CassandraClient() {

  val config = ConfigFactory.load() 
  val node = config.getString("cassandra.host") 

  // Connect directly to Cassandra from the driver
  private val cluster = Cluster.builder().addContactPoint(node).build()
  // log(cluster.getMetadata())
  private val session = cluster.connect()
  println(s"Connected to cluster ${cluster.getMetadata().getClusterName()}")


  def createSchema(): Unit = {
    // query_types Table
    // =================
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.query_types (
        event text,
        querytype text,
        PRIMARY KEY (event, querytype));""")
    // lambda_types Table
    // ==================
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.lambda_types (
        event text,
        querytype text,
        lambdatype text,
        PRIMARY KEY ((event, querytype), lambdatype));""")
    // queries Table
    // =============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.queries (
        event text,
        querytype text,
        lambdatype text,
        name text,
        lowertime text,
        uppertime text,
        trendtype text,
        value text,
        running boolean,
        speed_job_id text,
        batch_job_id text,
        serving_job_id text,
        PRIMARY KEY (event, querytype, lambdatype, name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS queries_name ON epic_realtime.queries (name);""")
  }


  def getEvent(event_name: String): Long = {
    session.execute("SELECT count(*) FROM epic_realtime.events WHERE event_name ='" + event_name + "';").one.getLong(0)
  }


  def createQuerySchema(query_name: String, querytype: String, lambdatype: String): Unit = {
      querytype match{
        case "dataset" => createDatasetQuerySchema(query_name, lambdatype)
        case "trend" => createTrendQuerySchema(query_name, lambdatype)
        case "popular" => 
          lambdatype match{
            case "tweet" =>  createPopularQuerySchema_tweet(query_name)
            case "user" =>  createPopularQuerySchema_user(query_name)
          }
      }
   }

  def createDatasetQuerySchema(query_name: String, lambdatype: String): Unit = {
    //Unique Query
    //=============
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        ${lambdatype} text, 
        PRIMARY KEY (${lambdatype}));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name};""") 

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_realtime(
        ${lambdatype} text, 
        PRIMARY KEY (${lambdatype}));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_realtime;""") 

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_batch(
        ${lambdatype} text, 
        PRIMARY KEY (${lambdatype}));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_batch;""") 

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp1(
        ${lambdatype} text, 
        PRIMARY KEY (${lambdatype}));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp1;""") 
        
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp2(
        ${lambdatype} text, 
        PRIMARY KEY (${lambdatype}));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp2;""")   
  }

  def createTrendQuerySchema(query_name: String, lambdatype: String): Unit = {
    //Trend Query
    //=============
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        ${lambdatype} text, 
        count int, 
        PRIMARY KEY (${lambdatype}, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name};""")  

   session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_realtime(
        ${lambdatype} text, 
        count int, 
        PRIMARY KEY (${lambdatype}, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_realtime;""")  

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_batch(
        ${lambdatype} text, 
        count int, 
        PRIMARY KEY (${lambdatype}, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_batch;""")  

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp1(
        ${lambdatype} text, 
        count int, 
        PRIMARY KEY (${lambdatype}, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp1;""")  
 
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp2(
        ${lambdatype} text, 
        count int, 
        PRIMARY KEY (${lambdatype}, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp2;""")  

  }

  def createPopularQuerySchema_tweet(query_name: String): Unit = {
    //Popular Tweet Query
    //===================
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        tweet_id text,
        user text,
        tweet_text text,
        count int,
        PRIMARY KEY ((tweet_id, count), user, tweet_text));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name};""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_realtime(
        tweet_id text,
        user text,
        tweet_text text,
        count int,
        PRIMARY KEY ((tweet_id, count), user, tweet_text));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_realtime;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_batch(
        tweet_id text,
        user text,
        tweet_text text,
        count int,
        PRIMARY KEY ((tweet_id, count), user, tweet_text));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_batch;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp1(
        tweet_id text,
        user text,
        tweet_text text,
        count int,
        PRIMARY KEY ((tweet_id, count), user, tweet_text));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp1;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp2(
        tweet_id text,
        user text,
        tweet_text text,
        count int,
        PRIMARY KEY ((tweet_id, count), user, tweet_text));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp2;""")
    
  }


  def createPopularQuerySchema_user(query_name: String): Unit = {
    //Popular User Query
    //==================
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        user text,
        count int,
        PRIMARY KEY (user, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name};""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_realtime(
        user text,
        count int,
        PRIMARY KEY (user, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_realtime;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_batch(
        user text,
        count int,
        PRIMARY KEY (user, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_batch;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp1(
        user text,
        count int,
        PRIMARY KEY (user, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp1;""")

    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}_tmp2(
        user text,
        count int,
        PRIMARY KEY (user, count));""")
    session.execute(
      s"""TRUNCATE epic_realtime.${query_name}_tmp2;""")
          
  }




  def getAsyncQueryResult(table_name: String, max: Int = 100) : ResultSetFuture = {
    session.executeAsync(QueryBuilder.select().all().from("epic_realtime", table_name).limit(max))
  }



  def addQueryType(event: String, querytype: String) = {
    session.execute(
      "INSERT INTO epic_realtime.query_types (event, querytype) " +
      "VALUES (" +
        "'" + event + "'," +
        "'" + querytype + "'" +
        ");")
  }
  def getQueryTypes: ResultSet = {
    session.execute("SELECT * FROM epic_realtime.query_types;")
  }
  def deleteQueryType(event: String, querytype: String) = {
    session.execute("DELETE FROM epic_realtime.query_types" +
    " WHERE event ='" + event + "' AND querytype ='" + querytype + "';")
  }



  def addLambdaType(event: String, querytype: String, lambdatype: String) = {
    session.execute(
      "INSERT INTO epic_realtime.lambda_types (event, querytype, lambdatype) " +
      "VALUES (" +
        "'" + event + "'," +
        "'" + querytype + "'," +
        "'" + lambdatype + "'" +
        ");")
  }
  def getLambdaTypes(event: String, querytype: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.lambda_types" +
      " WHERE event ='" + event + "' AND querytype ='" + querytype + "';")
  }
  def deleteLambdaType(event: String, querytype: String, lambdatype: String) = {
    session.execute("DELETE FROM epic_realtime.lambda_types" +
      " WHERE event ='" + event + "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "';")
  }


  def addQuery(name: String, event: String, querytype: String, lambdatype: String, 
    lowertime: String, uppertime: String, trendtype: String, value: String, running: Boolean, 
    speed_job_id: String, batch_job_id: String, serving_job_id: String) = {
    session.execute(
      "INSERT INTO epic_realtime.queries (name, event, querytype, lambdatype, lowertime, uppertime, " +
      "trendtype, value, running, speed_job_id, batch_job_id, serving_job_id) " +
      "VALUES (" +
        "'" + name + "'," +
        "'" + event+ "'," +
        "'" + querytype + "'," +
        "'" + lambdatype + "'," +
        "'" + lowertime + "'," +
        "'" + uppertime + "'," +
        "'" + trendtype + "'," +
        "'" + value + "'," +
        running + "," +
        "'" + speed_job_id + "'," +
        "'" + batch_job_id + "'," +
        "'" + serving_job_id + "'" +
        ");")
  }
  def getQuery(name: String): Row = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE name ='" + name + "';").one
  }
  def deleteQuery(event: String, querytype: String, lambdatype: String, name: String) = {
    session.execute("DELETE FROM epic_realtime.queries WHERE event ='" + event + 
      "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "' AND name ='" + name + "';")
  }
  def getQueries(event: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE event ='" + event + "';")
  }
  def getQueriesByQueryType(event: String, querytype: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE event ='" + event + "' AND querytype ='" + querytype + "';")
  }  
  def getQueriesByLambdaType(event: String, querytype: String, lambdatype: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE event ='" + event + "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "';")
  }
  def setQueryStatus(event: String, querytype: String, lambdatype: String, name: String, running: Boolean) = {
    session.execute("UPDATE epic_realtime.queries SET running = " + running + " WHERE event ='" + event + 
      "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "' AND name ='" + name + "';")
  }  
  def getQueryStatus(name: String) = {
    session.execute("SELECT running FROM epic_realtime.queries WHERE name ='" + name + "';").one.getBool(0)
  } 
  def setQuerySpeedJobId(event: String, querytype: String, lambdatype: String, name: String, id: String) = {
    session.execute("UPDATE epic_realtime.queries SET speed_job_id = '" + id + "' WHERE event ='" + event + 
      "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "' AND name ='" + name + "';")
  }
  def getQuerySpeedJobId(name: String): String = {
    session.execute("SELECT speed_job_id FROM epic_realtime.queries WHERE name ='" + name + "';").one.getString(0)
  }
  def setQueryBatchJobId(event: String, querytype: String, lambdatype: String, name: String, id: String) = {
    session.execute("UPDATE epic_realtime.queries SET batch_job_id = '" + id + "' WHERE event ='" + event + 
      "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "' AND name ='" + name + "';")
  }
  def getQueryBatchJobId(name: String): String = {
    session.execute("SELECT batch_job_id FROM epic_realtime.queries WHERE name ='" + name + "';").one.getString(0)
  }
  def setQueryServingJobId(event: String, querytype: String, lambdatype: String, name: String, id: String) = {
    session.execute("UPDATE epic_realtime.queries SET serving_job_id = '" + id + "' WHERE event ='" + event + 
      "' AND querytype ='" + querytype + "' AND lambdatype ='" + lambdatype + "' AND name ='" + name + "';")
  }
  def getQueryServingJobId(name: String): String = {
    session.execute("SELECT serving_job_id FROM epic_realtime.queries WHERE name ='" + name + "';").one.getString(0)
  }  


  def close() {
    session.close
    cluster.close
  }

}
