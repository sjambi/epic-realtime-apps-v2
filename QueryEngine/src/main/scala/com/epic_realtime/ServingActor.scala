package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }
import scala.concurrent.duration._
import scala.concurrent.Future
import akka.util.Timeout
import akka.pattern.pipe
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import com.datastax.driver.core.Row
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

import LambdaActor.DatasetQueryResult
import LambdaActor.TrendQueryResult
import LambdaActor.PopularTweetQueryResult
import LambdaActor.PopularUserQueryResult

object ServingActor {
  def props(name: String) = Props(new ServingActor(name))

  case class Start(query_name: String, event: String, querytype: String, lambdatype: String, 
    lowertime: String, uppertime: String, trendtype: String, value: String)
  case class Stop(query_name: String, event: String, querytype: String, lambdatype: String)
  case object Cancel
  case class GetResult(query_name: String, event: String, querytype: String, lambdatype: String)

}

class ServingActor(name: String) extends Actor {
  import ServingActor._
  import context._

  def buildDatasetHashtagRow(r: Row): DatasetQueryResult = {
    val value = r.getString("hashtag")
    DatasetQueryResult(value)
  }

  def buildDatasetUrlRow(r: Row): DatasetQueryResult = {
    val value = r.getString("url")
    DatasetQueryResult(value)
  }

  def buildDatasetUserRow(r: Row): DatasetQueryResult = {
    val value = r.getString("user")
    DatasetQueryResult(value)
  }

  def buildDatasetMentionRow(r: Row): DatasetQueryResult = {
    val value = r.getString("mention")
    DatasetQueryResult(value)
  }

  def buildTrendHashtagRow(r: Row): TrendQueryResult = {
    val value = r.getString("hashtag")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildTrendUrlRow(r: Row): TrendQueryResult = {
    val value = r.getString("url")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildTrendUserRow(r: Row): TrendQueryResult = {
    val value = r.getString("user")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildTrendMentionRow(r: Row): TrendQueryResult = {
    val value = r.getString("mention")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildPopularTweetRow(r: Row): PopularTweetQueryResult = {
    val tweet_id = r.getString("tweet_id")
    val count = r.getInt("count")
    val user = r.getString("user")
    val tweet_text = r.getString("tweet_text")   
    PopularTweetQueryResult(tweet_id, count, user, tweet_text)
  }

  def buildPopularUserRow(r: Row): PopularUserQueryResult = {
    val user = r.getString("user")
    val count = r.getInt("count")
    PopularUserQueryResult(user, count)
  }

  override def preStart() {
    // println(s"........ ServingActor (${name}) is started with path ${context.self.path}.")
    println(s"........ ServingActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")
 

  def receive = {


    case Start(query_name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime) =>
      println(s"${event}: ServingActor ${name} is started.")
      val mainclass = "com.epic_realtime.ServingLayer"
      val jarfile = s"$querytype/$lambdatype/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Serving Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runQuery(app_root+jarfile, app_name, mainclass, "serving",
          "1", "5", event, query_name, trendtype, value, lowertime, uppertime)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQueryServingJobId(event, querytype, lambdatype, query_name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Serving Job file ${app_root+jarfile} does not exist.")
      }


    case Stop(query_name, event, querytype, lambdatype) =>
      println(s"${event}: ServingActor ${name} is stopped.")
      val cassandraClient = new CassandraClient()
      val serving_job_id = cassandraClient.getQueryServingJobId(query_name)
      if (serving_job_id.nonEmpty) SparkJob.killJob(event, serving_job_id)     
      cassandraClient.setQueryServingJobId(event, querytype, lambdatype, query_name, "")
      cassandraClient.close


    case Cancel =>
      self ! PoisonPill


    case GetResult(query_name, event, querytype, lambdatype) =>
      println(s"${event}: ServingActor ${name} is getting results.")
      val cassandraClient = new CassandraClient()     
      val resultFuture = cassandraClient.getAsyncQueryResult(query_name)
      cassandraClient.close
      querytype match {
        case "dataset" =>
          lambdatype match{
            case "hashtag" =>
              resultFuture map(_.all().map(buildDatasetHashtagRow).toVector.sortBy(_.value)) pipeTo sender 
            case "url" =>
              resultFuture map(_.all().map(buildDatasetUrlRow).toVector.sortBy(_.value)) pipeTo sender
            case "user" =>
              resultFuture map(_.all().map(buildDatasetUserRow).toVector.sortBy(_.value)) pipeTo sender
            case "mention" =>
              resultFuture map(_.all().map(buildDatasetMentionRow).toVector.sortBy(_.value)) pipeTo sender
          }  
        // case "trend" =>
        //   lambdatype match{
        //     case "hashtag" =>
        //       resultFuture map(_.all().map(buildTrendHashtagRow).toVector.sortBy(-_.count)) pipeTo sender 
        //     case "url" =>
        //       resultFuture map(_.all().map(buildTrendUrlRow).toVector.sortBy(-_.count)) pipeTo sender
        //     case "user" =>
        //       resultFuture map(_.all().map(buildTrendUserRow).toVector.sortBy(-_.count)) pipeTo sender
        //     case "mention" =>
        //       resultFuture map(_.all().map(buildTrendMentionRow).toVector.sortBy(-_.count)) pipeTo sender
        //   }   
        // case "popular" => 
        //   lambdatype match{
        //     case "tweet" =>
        //       resultFuture map(_.all().map(buildPopularTweetRow).toVector.sortBy(-_.count)) pipeTo sender 
        //     case "user" =>
        //       resultFuture map(_.all().map(buildPopularUserRow).toVector.sortBy(-_.count)) pipeTo sender
        //   }  
      } 

  }
}
