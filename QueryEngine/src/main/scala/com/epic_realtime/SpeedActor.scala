package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import java.io._


object SpeedActor {
  def props(name: String) = Props(new SpeedActor(name))

  case class Start(query_name: String, event: String, querytype: String, lambdatype: String, 
    lowertime: String, uppertime: String, trendtype: String, value: String)
  case class Stop(query_name: String, event: String, querytype: String, lambdatype: String)
  case object Cancel

}

class SpeedActor(name: String) extends Actor {
  import SpeedActor._

  override def preStart() {
    // println(s"........ SpeedActor (${name}) is started with path ${context.self.path}.")
    println(s"........ SpeedActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {

    case Start(query_name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime) =>
      println(s"${event}: SpeedActor ${name} is started.")
      val mainclass = "com.epic_realtime.SpeedLayer"
      val jarfile = s"$querytype/$lambdatype/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Speed Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runQuery(app_root+jarfile, app_name, mainclass, "speed",
          "1", "5", event, query_name, trendtype, value, lowertime, uppertime)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQuerySpeedJobId(event, querytype, lambdatype, query_name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Speed Job file ${app_root+jarfile} does not exist.")
      }
  

    case Stop(query_name, event, querytype, lambdatype) =>
      println(s"${event}: SpeedActor ${name} is stopped.")
      val cassandraClient = new CassandraClient()
      val speed_job_id = cassandraClient.getQuerySpeedJobId(query_name)
      if (speed_job_id.nonEmpty) SparkJob.killJob(event, speed_job_id)     
      cassandraClient.setQuerySpeedJobId(event, querytype, lambdatype, query_name, "")
      cassandraClient.close
 

    case Cancel =>
      self ! PoisonPill


  }
}
