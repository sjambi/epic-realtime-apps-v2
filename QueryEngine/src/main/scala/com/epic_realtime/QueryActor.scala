package com.epic_realtime

import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask

import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._


object QueryActor_Implicits {
  implicit val timeout = Timeout(30 seconds)
}
import QueryActor_Implicits._;

object QueryActor {
  def props(name: String) = Props(new QueryActor(name))
 
  case class Event(event: String = "")
  case class Querytype(qtype: String = "")
  case object SaveQueryType
  case object GetQueryType
  case object CancelQueryType

  case class CreateLambdaSupervisor(name: String, event: String, querytype: String, lambdatype: String, loaded: Boolean) 
  // case class InitializeSchema(name: String)
  case class GetLambdaSupervisor(name: String)
  case class GetLambdaSupervisors(event: String, querytype: String)
  case class CancelLambdaSupervisor(name: String)

  case class LambdaType(event: String, querytype: String, lambdatype: String)
  case class LambdaTypes(lambdas: Vector[LambdaType] = Vector.empty[LambdaType]) {
    def lambda_num: Int = { return lambdas.size }
  }

  sealed trait LambdaTypeResponse
  case class LambdaTypeCreated(lambda: LambdaType) extends LambdaTypeResponse 
  case object LambdaTypeExists extends LambdaTypeResponse 
  case object LambdaTypeNoEventError extends LambdaTypeResponse 

}

class QueryActor(name: String) extends Actor {
  import QueryActor._
  import context._

  var event = ""
  var querytype = ""

  def createLambdaSupervisor(name: String) = 
    context.actorOf(LambdaSupervisor.props(name), name) 

  def intialize_loadLambdaTypes() = {
    val cassandraClient = new CassandraClient()
    val lambdaTypeSet = cassandraClient.getLambdaTypes(event, querytype)
    cassandraClient.close
    for (q <- lambdaTypeSet) {
      var event = q.getString("event") 
      var querytype = q.getString("querytype")   
      var lambdatype = q.getString("lambdatype")       
      val name = s"${event}_${querytype}_${lambdatype}"
      self ! CreateLambdaSupervisor(name, event, querytype, lambdatype, true)
    }
  }

  def eventExists(event: String): Boolean = {
    val cassandraClient = new CassandraClient()
    val count = cassandraClient.getEvent(event)
    cassandraClient.close
    if (count > 0) return true else return false
  }


  def receive = {

    case Event(e) => 
      event = e

    case Querytype(qtype) => 
      querytype = qtype
      intialize_loadLambdaTypes

    case SaveQueryType =>
      println(s"${event}: query type $name is saved.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addQueryType(event, querytype)
      cassandraClient.close

    case GetQueryType =>
      sender() ! Some(QuerySupervisor.QueryType(event, querytype))
  
    case CancelQueryType =>
      //Cancel all lambda supervisors
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteQueryType(event, querytype)
      cassandraClient.close
      sender() ! Some(QuerySupervisor.QueryType(event, querytype))
      self ! PoisonPill

    case CreateLambdaSupervisor(name, event, querytype, lambdatype, loaded) =>
      if (eventExists(event)){ //already created
        def create() = {  
          val lambdaSupervisor = createLambdaSupervisor(name)
          lambdaSupervisor ! LambdaSupervisor.Event(event)
          lambdaSupervisor ! LambdaSupervisor.Querytype(querytype)
          lambdaSupervisor ! LambdaSupervisor.Lambdatype(lambdatype)
          if (!loaded) lambdaSupervisor ! LambdaSupervisor.SaveLambdaType
          sender() ! LambdaTypeCreated(LambdaType(event, querytype, lambdatype))
          println(s"\n${event}: Creating LambdaSupervisor as (${name}) with Actor Reference ${lambdaSupervisor}")
        }
        context.child(name).fold(create())(_ => sender() ! LambdaTypeExists)
      } else {
        sender() ! LambdaTypeNoEventError
      }  

    case GetLambdaSupervisor(name) =>
      def notFound() = sender() ! None
      def getQueryActor(child: ActorRef) = child forward LambdaSupervisor.GetLambdaType
      context.child(name).fold(notFound())(getQueryActor)


    case GetLambdaSupervisors(event, querytype) =>
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // Should return queries of the requested event only
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''  
      import akka.pattern.ask
      import akka.pattern.pipe
      def getLambdaSupervisors = context.children.map { child =>
        self.ask(GetLambdaSupervisor(child.path.name)).mapTo[Option[LambdaType]] 
      }
      def convertToLambdaTypes(f: Future[Iterable[Option[LambdaType]]]) =
        f.map(_.flatten).map(l=> LambdaTypes(l.toVector)) 
      pipe(convertToLambdaTypes(Future.sequence(getLambdaSupervisors))) to sender() 
 

    case CancelLambdaSupervisor(name) =>
      def notFound() = sender() ! None
      def cancelQueryType(child: ActorRef) = child forward LambdaSupervisor.CancelLambdaType
      context.child(name).fold(notFound())(cancelQueryType)
  

  }
}

