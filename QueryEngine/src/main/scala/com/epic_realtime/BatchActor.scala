package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import java.io._


object BatchActor {
  def props(name: String) = Props(new BatchActor(name))

  case class Start(query_name: String, event: String, querytype: String, lambdatype: String, 
    lowertime: String, uppertime: String, trendtype: String, value: String)
  case class Stop(query_name: String, event: String, querytype: String, lambdatype: String)
  case object Cancel

}

class BatchActor(name: String) extends Actor {
  import BatchActor._

  override def preStart() {
    // println(s"........ BatchActor (${name}) is started with path ${context.self.path}.")
    println(s"........ BatchActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {

    case Start(query_name, event, querytype, lambdatype, trendtype, value, lowertime, uppertime) =>
      println(s"${event}: BatchActor ${name} is started.")
      val mainclass = "com.epic_realtime.BatchLayer"
      val jarfile = s"$querytype/$lambdatype/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Batch Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runQuery(app_root+jarfile, app_name, mainclass, "batch",
          "1", "", event, query_name, trendtype, value, lowertime, uppertime)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQueryBatchJobId(event, querytype, lambdatype, query_name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Batch Job file ${app_root+jarfile} does not exist.")
      }
  

    case Stop(query_name, event, querytype, lambdatype) =>
      println(s"${event}: BatchActor ${name} is stopped.")
      val cassandraClient = new CassandraClient()
      val batch_job_id = cassandraClient.getQueryBatchJobId(query_name)
      if (batch_job_id.nonEmpty) SparkJob.killJob(event, batch_job_id)     
      cassandraClient.setQueryBatchJobId(event, querytype, lambdatype, query_name, "")
      cassandraClient.close
 

    case Cancel =>
      self ! PoisonPill


  }
}
