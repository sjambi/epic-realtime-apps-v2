package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._

object QuerySupervisor {
  def props(implicit timeout: Timeout) = Props(new QuerySupervisor)
  def name = "querySupervisor"

  case class CreateQueryActor(name: String, event: String, querytype: String, loaded: Boolean) 
  // case class InitializeSchema(name: String)
  case class GetQueryActor(name: String) 
  case class GetQueryActors(event: String)
  case class CancelQueryActor(name: String) 
 
  case class QueryType(event: String, querytype: String)
  case class QueryTypes(querytypes: Vector[QueryType] = Vector.empty[QueryType])

  sealed trait QueryTypeResponse 
  case class QueryTypeCreated(query: QueryType) extends QueryTypeResponse 
  case object QueryTypeExists extends QueryTypeResponse 
  case object QueryTypeNoEventError extends QueryTypeResponse 
}


class QuerySupervisor(implicit timeout: Timeout) extends Actor {
  import QuerySupervisor._
  import context._

  def createQueryActor(name: String) =
    context.actorOf(QueryActor.props(name), name) 

  def intialize_loadQueryTypes() = {
    val cassandraClient = new CassandraClient()
    cassandraClient.createSchema
    val queryTypeSet = cassandraClient.getQueryTypes
    cassandraClient.close
   for (q <- queryTypeSet) {
      var event = q.getString("event") 
      var querytype = q.getString("querytype")       
      val name = s"${event}_${querytype}"
      self ! CreateQueryActor(name, event, querytype, true)
    }
  }

  def eventExists(event: String): Boolean = {
    val cassandraClient = new CassandraClient()
    val count = cassandraClient.getEvent(event)
    cassandraClient.close
    if (count > 0) return true else return false
  }
  
  override def preStart() {
    // println(s"QuerySupervisor Actor is started with path ${context.self.path}.")
    intialize_loadQueryTypes
    println("SCHEMA CREATED.")
    println("\n-----------------------------------------\n")
  }

 
  def receive = {

    case CreateQueryActor(name, event, querytype, loaded) =>
      if (eventExists(event)){ //already created
        def create() = {  
          val queryActor = createQueryActor(name)
          queryActor ! QueryActor.Event(event)
          queryActor ! QueryActor.Querytype(querytype)
          if (!loaded) queryActor ! QueryActor.SaveQueryType
          sender() ! QueryTypeCreated(QueryType(event, querytype))
          println(s"\n${event}: Creating QueryActor as (${name}) with Actor Reference ${queryActor}")
        }
        context.child(name).fold(create())(_ => sender() ! QueryTypeExists)
      } else {
        sender() ! QueryTypeNoEventError
      }  


    case GetQueryActor(name) =>
      def notFound() = sender() ! None
      def getQueryActor(child: ActorRef) = child forward QueryActor.GetQueryType
      context.child(name).fold(notFound())(getQueryActor)


    case GetQueryActors(event) =>
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // Should return queries of the requested event only
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''  
      import akka.pattern.ask
      import akka.pattern.pipe
      def getQueryTypes = context.children.map { child =>
        self.ask(GetQueryActor(child.path.name)).mapTo[Option[QueryType]] 
      }
      def convertToQueryTypes(f: Future[Iterable[Option[QueryType]]]) =
        f.map(_.flatten).map(l=> QueryTypes(l.toVector)) 
      pipe(convertToQueryTypes(Future.sequence(getQueryTypes))) to sender() 
 

    case CancelQueryActor(name) =>
      def notFound() = sender() ! None
      def cancelQueryType(child: ActorRef) = child forward QueryActor.CancelQueryType
      context.child(name).fold(notFound())(cancelQueryType)


  }
}

