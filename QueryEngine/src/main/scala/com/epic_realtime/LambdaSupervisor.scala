package com.epic_realtime

import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask

import com.epic_realtime.utils.CassandraClient
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

object LambdaSupervisor_Implicits {
  implicit val timeout = Timeout(30 seconds)
}
import LambdaSupervisor_Implicits._;

object LambdaSupervisor {
  def props(name: String) = Props(new LambdaSupervisor(name))
 
  case class Event(name: String = "")
  case class Querytype(qtype: String = "")
  case class Lambdatype(ltype: String = "")
  case object SaveLambdaType
  case object GetLambdaType
  case object CancelLambdaType
  case class CreateLambdaActorSchema(name: String, querytype: String, lambdatype: String)
  case class CreateLambdaActor(name: String, event: String, querytype: String, lambdatype: String,
    lowertime: String, uppertime: String, trendtype: String, value: String, running: Boolean,
    speed_job_id: String, batch_job_id: String, serving_job_id: String, loaded: Boolean) 
  case class GetLambdaActor(name: String) 
  case class GetLambdaActors(event: String, querytype: String, lambdatype: String)
  case class CancelLambdaActor(name: String) 
  case class StartLambdaActor(name: String) 
  case class StopLambdaActor(name: String) 
  case class GetLambdaActorResult(name: String)

  case class LambdaQuery(name: String, event: String, querytype: String, lambdatype: String, 
    lowertime: String ="", uppertime: String ="", trendtype: String ="", value: String ="", 
    running: Boolean = false, speed_job_id: String ="", batch_job_id: String ="", serving_job_id: String ="")
  case class LambdaQueries(queries: Vector[LambdaQuery] = Vector.empty[LambdaQuery]) {
    def query_num: Int = { return queries.size }
  }

  sealed trait LambdaQueryResponse
  case class LambdaQueryCreated(query: LambdaQuery) extends LambdaQueryResponse 
  case object LambdaQueryExists extends LambdaQueryResponse 
  case object LambdaQueryNoEventError extends LambdaQueryResponse 

}


class LambdaSupervisor(name: String) extends Actor {
  import LambdaSupervisor._
  import context._

  var event = ""
  var querytype = ""
  var lambdatype = ""


  def createLambdaActor(name: String) = 
    context.actorOf(LambdaActor.props(name), name) 


  def intialize_loadQueries() = {
    val cassandraClient = new CassandraClient()
    val querySet = cassandraClient.getQueriesByLambdaType(event, querytype, lambdatype)
    cassandraClient.close
    for (q <- querySet) {
      var name = q.getString("name")
      var event = q.getString("event") 
      var querytype = q.getString("querytype")       
      var lambdatype = q.getString("lambdatype")
      var lowertime = q.getString("lowertime")
      var uppertime = q.getString("uppertime")
      var trendtype = q.getString("trendtype")
      var value = q.getString("value")
      var running = q.getBool("running")
      var speed_job_id = q.getString("speed_job_id")
      var batch_job_id = q.getString("batch_job_id")
      var serving_job_id = q.getString("serving_job_id")
      self ! CreateLambdaActor(name, event, querytype, lambdatype, lowertime, uppertime, trendtype, value, 
      running, speed_job_id, batch_job_id, serving_job_id, true)
    }
  }

  def eventExists(event: String): Boolean = {
    val cassandraClient = new CassandraClient()
    val count = cassandraClient.getEvent(event)
    cassandraClient.close
    if (count > 0) return true else return false
  }
  
 
  def receive = {

    case Event(e) => event = e

    case Querytype(qtype) => querytype = qtype
 
    case Lambdatype(attr) =>  
      lambdatype = attr
      intialize_loadQueries


    case SaveLambdaType =>
      println(s"${event}: lambda type $name is saved.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addLambdaType(event, querytype, lambdatype)
      cassandraClient.close  

    case GetLambdaType =>
      sender() ! Some(QueryActor.LambdaType(event, querytype, lambdatype))
  
    case CancelLambdaType =>
      //Cancel all lambda actors
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteLambdaType(event, querytype, lambdatype)
      cassandraClient.close      
      sender() ! Some(QueryActor.LambdaType(event, querytype, lambdatype))
      self ! PoisonPill

    case CreateLambdaActorSchema(name, querytype, lambdatype) =>     
      println(s"${event}: query schema is created for $name.")
      println("\n------------------------------------------------\n")
      val cassandraClient = new CassandraClient()
      cassandraClient.createQuerySchema(name, querytype, lambdatype)
      cassandraClient.close
 
    case CreateLambdaActor(name, event, querytype, lambdatype, lowertime, uppertime, trendtype, value, 
      running, speed_job_id, batch_job_id, serving_job_id, loaded) =>
      if (eventExists(event)){ //already created
        def create() = {  
          val queryActor = createLambdaActor(name)
          queryActor ! LambdaActor.Event(event)
          queryActor ! LambdaActor.Querytype(querytype)
          queryActor ! LambdaActor.Lambdatype(lambdatype)
          queryActor ! LambdaActor.Lowertime(lowertime)
          queryActor ! LambdaActor.Uppertime(uppertime)
          queryActor ! LambdaActor.Trendtype(trendtype)
          queryActor ! LambdaActor.Value(value)
          queryActor ! LambdaActor.Running(running)
          queryActor ! LambdaActor.Speedjobid(speed_job_id)
          queryActor ! LambdaActor.Batchjobid(batch_job_id)
          queryActor ! LambdaActor.Servingjobid(serving_job_id)
          if (!loaded){
            self ! CreateLambdaActorSchema(name, querytype, lambdatype)
            queryActor ! LambdaActor.SaveQuery
          }
          sender() ! LambdaQueryCreated(LambdaQuery(name, event, querytype, lambdatype, lowertime, uppertime, trendtype, value, 
            running, speed_job_id, batch_job_id, serving_job_id))
          println(s"\n${event}: Creating LambdaActor as (${name}) with Actor Reference ${queryActor}")
        }
        context.child(name).fold(create())(_ => sender() ! LambdaQueryExists)
      } else {
        sender() ! LambdaQueryNoEventError
      }  


    case GetLambdaActor(name) =>
      def notFound() = sender() ! None
      def getLambdaQuery(child: ActorRef) = child forward LambdaActor.GetQuery
      context.child(name).fold(notFound())(getLambdaQuery)


    case GetLambdaActors(event, querytype, lambdatype) =>
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // Should return queries of the requested event only
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''  
      import akka.pattern.ask
      import akka.pattern.pipe
      def getLambdaActors = context.children.map { child =>
        self.ask(GetLambdaActor(child.path.name)).mapTo[Option[LambdaQuery]] 
      }
      def convertToLambdaActors(f: Future[Iterable[Option[LambdaQuery]]]) =
        f.map(_.flatten).map(l=> LambdaQueries(l.toVector)) 
      pipe(convertToLambdaActors(Future.sequence(getLambdaActors))) to sender() 
 

    case CancelLambdaActor(name) =>
      def notFound() = sender() ! None
      def cancelLambdaQuery(child: ActorRef) = child forward LambdaActor.CancelQuery
      context.child(name).fold(notFound())(cancelLambdaQuery)


    case StartLambdaActor(name) =>
      def notFound() = sender() ! None
      def startLambdaQuery(child: ActorRef) = child forward LambdaActor.StartQuery
      context.child(name).fold(notFound())(startLambdaQuery)


    case StopLambdaActor(name) =>
      def notFound() = sender() ! None
      def stopLambdaQuery(child: ActorRef) = child forward LambdaActor.StopQuery
      context.child(name).fold(notFound())(stopLambdaQuery)


    case GetLambdaActorResult(name) =>
      def notFound() = sender() ! None
      def getLambdaQueryResult(child: ActorRef) = child forward LambdaActor.GetQueryResult
      context.child(name).fold(notFound())(getLambdaQueryResult)

  }
}

