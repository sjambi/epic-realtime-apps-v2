package com.epic_realtime

import com.epic_realtime.SparkCommon._
import org.apache.spark.{SparkConf, SparkContext}
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._


object ServingLayer_tmp {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def merge(conf: SparkConf, sc: SparkContext, query_name: String, query_view: String, trend: String, value: String) {
 
    // val conf = SparkCommon.conf(query_name, "1")
    // val sc = SparkCommon.sc(conf)
    // // val ssc = SparkCommon.ssc(sc, seconds.toInt)

    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    // Combine data from realtime and batch views using union
    val viewRDD = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_${query_view}")
    val queryRDD = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}")
    val unionRDD = queryRDD.union(viewRDD)
    // unionRDD.cache()

    // println("\n\n\n============================================\n")
    // println(s"RDD COUNT = ${unionRDD.count}")
    // println("\n============================================\n\n\n")

    val d1 = DateTime.now

    // Convert RDD to Data Frame objects
    val sqlContext = new SQLContext(sc)
    import sqlContext.implicits._

    // Generate schema
    val schema = new StructType(Array(
      StructField("hashtag", StringType, nullable=false),
      StructField("count", IntegerType, nullable=false)
    ))
    // Convert RDD to Cassandra Rows
    val rowRDD = unionRDD.map(r => Row(r.get[String]("hashtag"), r.get[Int]("count")))
    // Apply schema to Rows
    val hashtagDF = sqlContext.createDataFrame(rowRDD, schema)
    // hashtagDF.printSchema()

    // Group by hashtag values and add up their counts
    val hashtagCount = hashtagDF
      .groupBy("hashtag")
      .agg(sum("count").alias("count"))
      .sort(desc("count"),asc("hashtag"))

    // Check user's input
    val hashtagTrend =
    trend match {
      case "threshold" => hashtagCount.where("count>="+value)
      case "topk" => hashtagCount.limit(value.toInt)
      case _ => hashtagCount.limit(10)
    }
    hashtagTrend.show()

    // Save results as a query result view
    hashtagTrend.write
      .format("org.apache.spark.sql.cassandra")
      .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
      .mode(SaveMode.Overwrite)
      .save()
   
    val d2 = DateTime.now
    val processingTime = (d2.getMillis()-d1.getMillis()).toFloat/1000
       
    val data = s"Serving: ${queryRDD.count}, ${query_view}|${viewRDD.count}, ${processingTime}"
    producer.send(new ProducerRecord[String, String] (topic, data))

  }
}
