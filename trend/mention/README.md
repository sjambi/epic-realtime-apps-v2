hashtag
=======
This app contains three types of Spark jobs that create a Lambda Architecture process for Trend Queries that flexibly find trending hashtags in Twitter streaming and historical data of a requested event.


************** Spark Jobs ***************


SpeedLayer
----------
Creates a hashtag real-time view for the requested event. It consumes Twitter data streaming from event's Kafka topic, extracts hashtags found in tweet's text along with the created_at value, then stores a tuple of (event_name, time, hashtag) into a Cassandra table called "hashtags_realtime".

com.epic_realtime.SpeedLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.SpeedLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



BatchLayer
----------
Creates a hashtag batch view for the requested event. It retrieves Twitter historical data stored in a Cassandra table called "tweets" that contains all previously collected tweet raw data for all events. It filters in event's tweets, reads the created_at column, extracts hashtags from the tweet_json column, then stores a tuple of (event_name, time, hashtag) into a Cassandra table called "hashtags_batch".

com.epic_realtime.BatchLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.BatchLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



ServingLayer
------------
Creates a hashtag query result of realtime and batch hashtag aggregates values for the requested event. It retrieves real-time and batch hashtags data stored in Cassandra tables "hashtags_realtime" and "hashtags_batch", merges them, filters data based on a requested time period, then generates a list of trending hashtags with their counts, either by a threshold that determines if a hashtag is considered a trend or a topk value of the most frequently posted hashtags.
The output is saved in a new Cassandra table named by the query's generated name, updated every new data streams in. 

com.epic_realtime.ServingLayer <event_name> <query_name> 
<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]> 

Note: 
- if trendtype and trendvalue are not provided, the default values are topk 10
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.ServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_hashtag_01 threshold 10 2016-07-02 2016-07-03 



DatasetServingLayer
-------------------
Creates a hashtag query result of realtime and batch distinct hashtag values for the requested event. It retrieves real-time and batch hashtags data stored in Cassandra tables "hashtags_realtime" and "hashtags_batch", merges them, filters data based on a requested time period, then generates a list of unique hashtags.
The output is saved in a new Cassandra table named by the query's generated name, updated every new data streams in. 

com.epic_realtime.DatasetServingLayer <event_name> <query_name> 
<start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]> 

Note: 
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.DatasetServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_hashtag_01 2016-07-02 2016-07-03 

