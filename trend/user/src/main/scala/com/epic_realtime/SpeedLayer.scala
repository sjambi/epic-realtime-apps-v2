package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.epic_realtime.ServingLayer._

// Spark Streaming + Kafka imports
import kafka.serializer.StringDecoder // this has to come before streaming.kafka import
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import com.github.nscala_time.time.Imports._



object SpeedLayer {

  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: com.epic_realtime.SpeedLayer <cores> <seconds> <event_name> <query_name> " +
        "<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }
    
    val cores = args(0)
    val seconds = args(1)
    val event = args(2) 
    val query_name = args(3)
    val trend = if (args.length > 4) args(4) else "topk"   //default value
    val value = if (args.length > 5) args(5) else "10"     //default value
    val start_date = if (args.length > 6) args(6) else "" 
    val end_date = if (args.length > 7) args(7) else "" 

    val conf = SparkCommon.conf(s"${query_name}_speed", cores)
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, seconds.toInt)
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    // Query Date Entry:
    // - Now           =>   start_date = "",          end_date = ""
    // - Since         =>   start_date = user_input,  end_date = ""
    // - Time Period   =>   start_date = user_input,  end_date = user_input

    if (end_date.nonEmpty){
      println("No Speed Job Required.")
      producer.send(new ProducerRecord[String, String] (topic, "No Speed Job Required."))
      // Clean up any old realtime view 
      CassandraConnector(conf).withSessionDo { session =>
        session.execute(s"TRUNCATE epic_realtime.${query_name.toLowerCase()}_realtime")
      }
    } else {

      // Start up a speed job
      //.................................................................................
      
      // Kafka configuration
      val kafkaConf = SparkCommon.kafkaConf
      val kafka_topic = event
      val topicsSet = Set[String] (kafka_topic)

      val directKafkaStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
        ssc, kafkaConf, topicsSet).map(_._2)
      // directKafkaStream.cache()

      val sqlContext = new SQLContext(sc)
      import sqlContext.implicits._

      directKafkaStream.foreachRDD(rdd => {
        println(s"\nReceived tweets in $seconds second(s) (%s total):".format(rdd.count()))

        if (rdd.count>0){

          val d1 = DateTime.now

          // Extract user values
          val df = sqlContext.read.json(rdd)
          val users = df.select(df("user.screen_name"))

          // Save results as a realtime view
          //................................
          // Save results as a temp view          
          users.map(x => (x.getString(0),1)).reduceByKey(_+_)//.reduceByKey((x, y) => x + y)
          .toDF.select(col("_1").alias("user"), col("_2").alias("count"))
          .write
            .format("org.apache.spark.sql.cassandra")
            .options(Map( "table" -> s"${query_name.toLowerCase()}_tmp1", "keyspace" -> "epic_realtime"))
            .mode(SaveMode.Overwrite)
            .save()

          println("SAVE TO REALTIME TMP1 FILE")

          // Combine realtime and temp views
          //.............................
          val old_result = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_realtime")
          val new_result = sc.cassandraTable("epic_realtime", s"${query_name.toLowerCase()}_tmp1")         
          val union_result = old_result.union(new_result)
          union_result.map(row => (row.getString("user"), row.getInt("count")))
            .reduceByKey(_+_)
            .toDF.select(col("_1").alias("user"), col("_2").alias("count"))
            .write
            .format("org.apache.spark.sql.cassandra")
            .options(Map( "table" -> s"${query_name.toLowerCase()}_realtime", "keyspace" -> "epic_realtime"))
            .mode(SaveMode.Overwrite)
            .save()
        
          println("SAVE TO REALTIME FILE FROM REALTIME AND TMP1")

          val d2 = DateTime.now

          // Get a tweet time
          val tweet_time = df.select(df("created_at")).first().getString(0)
          val pattern = "EEE MMM dd HH:mm:ss Z yyyy"  //"Sat Oct 29 06:42:39 +0000 2016"
          val d3 = DateTime.parse(tweet_time, DateTimeFormat.forPattern(pattern))
          
          val processingTime = (d2.getMillis()-d1.getMillis()).toFloat/1000
          val immediacyTime =  (d2.getMillis()-d3.getMillis()).toFloat/1000

          producer.send(new ProducerRecord[String, String] 
            (topic, s"Speed: ${rdd.count}, ${processingTime}, ${immediacyTime}"))

          // Call Serving Job
          // ServingLayer_tmp.merge(conf, sc, query_name, "realtime", trend, value)


        }//rdd.count
      })

      ssc.start()
      // timer.start()
      ssc.awaitTermination()
      // ssc.stop()
    }//speed-job-end

  }
}
